import { Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Cron, CronExpression } from '@nestjs/schedule';
import { InjectRepository } from '@nestjs/typeorm';
import axios, { AxiosRequestConfig } from 'axios';
import { Order } from 'src/common/database/entities/Order';

import { OrderService } from 'src/order/order.service';
import { Repository } from 'typeorm';

@Injectable()
export class TaskService {
  private readonly logger = new Logger(TaskService.name);
  private readonly url_api_get_detail_url;
  private readonly shop_id_dev;
  private readonly api_key_dev;
  private readonly config: AxiosRequestConfig;
  constructor(
    private readonly orderService: OrderService,
    @InjectRepository(Order)
    private readonly orderRepository: Repository<Order>,
    private readonly configService: ConfigService,
  ) {
    this.api_key_dev = this.configService.get('API_KEY_DEV');
    this.shop_id_dev = this.configService.get('SHOP_ID_DEV');
    this.url_api_get_detail_url = this.configService.get(
      'URL_API_GET_DETAIL_ORDER',
    );
    this.config = {
      headers: {
        'Content-Type': 'application/json',
        token: this.api_key_dev,
        ShopId: this.shop_id_dev,
      },
    };
  }

  @Cron(CronExpression.EVERY_2_HOURS)
  async handleCron() {
    this.logger.debug('----------Called every 10 seconds---------');
    await this.updateOrderStatuses();
  }

  async onModuleInit() {
    this.logger.debug('TaskService initialized');
    await this.updateOrderStatuses();
  }

  async triggerOrderStatusUpdate(userId: string) {
    this.logger.debug('TaskService User:' + userId);
    await this.updateOrderStatusByUser(userId);
  }

  async updateOrderStatus(orders: Order[]) {
    for (const order of orders) {
      try {
        if (order.orderCode == null) {
          continue;
        } else {
          const dataStatus = {
            order_code: order.orderCode,
          };
          const response = await axios.post(
            this.url_api_get_detail_url,
            dataStatus,
            this.config,
          );
          order.status = response.data.data.status;
          await this.orderRepository.save(order);
        }
      } catch (error) {
        this.logger.error(
          `Failed to update order ${order.id}: ${
            error.response ? error.response.data : error.message
          }`,
        );
      }
    }
  }

  async updateOrderStatuses() {
    const orders = await this.orderRepository.find();
    await this.updateOrderStatus(orders);
  }

  async updateOrderStatusByUser(userId: string) {
    const orders = await this.orderRepository.find({
      where: { userId: userId },
    });
    await this.updateOrderStatus(orders);
  }
}
