import { Module } from '@nestjs/common';
import { TaskService } from './task.service';
import { TaskController } from './task.controller';
import { OrderService } from 'src/order/order.service';
import { CartService } from 'src/cart/cart.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Cart } from 'src/common/database/entities/Cart';
import { User } from 'src/common/database/entities/User';
import { Order } from 'src/common/database/entities/Order';
import { Shipping } from 'src/common/database/entities/Shipping';
import { Address } from 'src/common/database/entities/Address';
import { Store } from 'src/common/database/entities/Store';
import { FeeShippingService } from 'src/fee-shipping/fee-shipping.service';
import { ConfigService } from '@nestjs/config';
import { CartItem } from 'src/common/database/entities/CartItem';
import { Product } from 'src/common/database/entities/Product';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      Cart,
      User,
      Order,
      Shipping,
      Address,
      Store,
      CartItem,
      Product,
    ]),
  ],
  controllers: [TaskController],
  providers: [
    TaskService,
    OrderService,
    CartService,
    FeeShippingService,
    ConfigService,
  ],
})
export class TaskModule {}
