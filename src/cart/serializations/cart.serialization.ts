import { Cart } from 'src/common/database/entities/Cart';
import { CartItem } from 'src/common/database/entities/CartItem';
import { AddProductResponse } from '../interfaces/cart.add-product-response.interface';
import { ApiProperty } from '@nestjs/swagger';
import { faker } from '@faker-js/faker';
import { DeleteResponse } from 'src/common/helper/interfaces/helper.response-delete';

export class CartResponseSerialization {
  @ApiProperty({
    example: faker.string.uuid(),
    required: true,
    nullable: false,
  })
  id: string;

  @ApiProperty({
    example: faker.string.uuid(),
    required: true,
    nullable: false,
  })
  userId: string;

  @ApiProperty({
    example: faker.string.uuid(),
    required: true,
    nullable: false,
  })
  shopOrderId: string;

  @ApiProperty({
    example: faker.date.birthdate(),
    required: true,
    nullable: false,
  })
  createdAt: Date;

  @ApiProperty({
    example: faker.date.birthdate(),
    required: true,
    nullable: false,
  })
  updatedAt: Date;
}

export class CartItemResponseSerialization {
  @ApiProperty({
    example: faker.string.uuid(),
    required: true,
    nullable: false,
  })
  id: string;

  @ApiProperty({
    example: faker.string.uuid(),
    required: true,
    nullable: false,
  })
  cartId: string;

  @ApiProperty({
    example: faker.string.uuid(),
    required: true,
    nullable: false,
  })
  productId: string;

  @ApiProperty({
    example: faker.number.int(100),
    required: true,
    nullable: false,
  })
  quantity: number;

  @ApiProperty({
    example: faker.date.birthdate(),
    required: true,
    nullable: false,
  })
  createdAt: Date;

  @ApiProperty({
    example: faker.date.birthdate(),
    required: true,
    nullable: false,
  })
  updatedAt: Date;
}

export class CartCreateSerialization implements AddProductResponse {
  @ApiProperty({
    type: CartResponseSerialization,
    required: true,
    nullable: false,
  })
  Cart: Cart;

  @ApiProperty({
    type: CartItemResponseSerialization,
    required: true,
    nullable: false,
  })
  CartItem: CartItem;
}

export class CartUpdateSerialization extends CartCreateSerialization {}

export class CartGetDetailSerialization {
  @ApiProperty({
    type: CartResponseSerialization,
    required: true,
    nullable: false,
  })
  Cart: Cart;

  @ApiProperty({
    type: [CartItemResponseSerialization],
    required: true,
    nullable: false,
  })
  CartItem: CartItem[];
}

export class CartDeleteSerialization implements DeleteResponse<Cart> {
  @ApiProperty({
    example: 'delete product successful',
  })
  message: string;
  @ApiProperty({
    type: [CartResponseSerialization],
    required: true,
    nullable: false,
  })
  data: Cart;
}
