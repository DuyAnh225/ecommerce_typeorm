import { applyDecorators } from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiParam,
  ApiResponse,
} from '@nestjs/swagger';
import {
  CartCreateSerialization,
  CartDeleteSerialization,
  CartGetDetailSerialization,
  CartUpdateSerialization,
} from '../serializations/cart.serialization';
import { faker } from '@faker-js/faker';

export function CartCreateDoc(): MethodDecorator {
  return applyDecorators(
    ApiBearerAuth(),
    ApiOperation({ summary: 'module cart' }),
    ApiResponse({
      status: 201,
      description: 'Success',
      type: CartCreateSerialization,
    }),
  );
}

export function CartUpdateDoc(): MethodDecorator {
  return applyDecorators(
    ApiBearerAuth(),
    ApiOperation({ summary: 'module cart' }),
    ApiParam({
      name: 'id',
      example: faker.string.uuid(),
      type: 'string',
    }),
    ApiResponse({
      status: 200,
      description: 'Success',
      type: CartUpdateSerialization,
    }),
  );
}

export function CartGetDetailDoc(): MethodDecorator {
  return applyDecorators(
    ApiBearerAuth(),
    ApiOperation({ summary: 'module cart' }),
    ApiParam({
      name: 'id',
      example: faker.string.uuid(),
      type: 'string',
    }),
    ApiResponse({
      status: 200,
      description: 'Success',
      type: CartGetDetailSerialization,
    }),
  );
}

export function CartDeleteDoc(): MethodDecorator {
  return applyDecorators(
    ApiBearerAuth(),
    ApiOperation({ summary: 'module cart' }),
    ApiParam({
      name: 'id',
      example: faker.string.uuid(),
      type: 'string',
    }),
    ApiResponse({
      status: 200,
      description: 'Success',
      type: CartDeleteSerialization,
    }),
  );
}
