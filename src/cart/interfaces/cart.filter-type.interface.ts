export interface CartFilterType {
  items_per_page?: number;
  page?: number;
  sort_by?: string;
  order_by?: string;
  userId?: string;
  orderId?: string;
  isOrder?: boolean;
}
