import { Cart } from 'src/common/database/entities/Cart';
import { CartItem } from 'src/common/database/entities/CartItem';

export interface AddProductResponse {
  Cart: Cart;
  CartItem: CartItem;
}
