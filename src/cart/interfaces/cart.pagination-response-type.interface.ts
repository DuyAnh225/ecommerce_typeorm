export interface CartPaginationResponseType<T> {
  data: T;
  total: number;
  currentPage: number;
  itemsPerPage: number;
}
