import { Cart } from 'src/common/database/entities/Cart';
import { CartItem } from 'src/common/database/entities/CartItem';

export interface CartGetDetailResponse {
  Cart: Cart;
  Items: CartItem[];
}
