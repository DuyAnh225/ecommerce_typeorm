import { Module } from '@nestjs/common';
import { CartService } from './cart.service';
import { CartController } from './cart.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Feedback } from 'src/common/database/entities/Feedback';
import { Cart } from 'src/common/database/entities/Cart';
import { CartItem } from 'src/common/database/entities/CartItem';
import { Product } from 'src/common/database/entities/Product';
import { User } from 'src/common/database/entities/User';
import { Store } from 'src/common/database/entities/Store';

@Module({
  imports: [TypeOrmModule.forFeature([Feedback, Cart, CartItem, Product,User,Store])],
  controllers: [CartController],
  providers: [CartService],
})
export class CartModule {}
