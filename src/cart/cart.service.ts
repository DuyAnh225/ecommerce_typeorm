import {
  BadRequestException,
  HttpStatus,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Cart } from 'src/common/database/entities/Cart';
import { CartItem } from 'src/common/database/entities/CartItem';
import { Product } from 'src/common/database/entities/Product';
import { Store } from 'src/common/database/entities/Store';
import { Brackets, Repository } from 'typeorm';
import { AddProductDto } from './dtos/cart.add-product.dto';

import { User } from 'src/common/database/entities/User';

import { AddProductResponse } from './interfaces/cart.add-product-response.interface';
import { UpdateCartDto } from './dtos/cart.update.dto';
import { CartGetDetailResponse } from './interfaces/cart.get-detail-response.interface';
import { DeleteResponse } from 'src/common/helper/interfaces/helper.response-delete';
import { CartFilterType } from './interfaces/cart.filter-type.interface';
import { SORT_BY_CART } from './constants/product.sort-by.constants';

@Injectable()
export class CartService {
  constructor(
    @InjectRepository(Cart) private readonly cartRepository: Repository<Cart>,
    @InjectRepository(CartItem)
    private readonly cartItemRepository: Repository<CartItem>,
    @InjectRepository(Product)
    private readonly productRepository: Repository<Product>,
    @InjectRepository(Store)
    private readonly storeRepository: Repository<Store>,
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
  ) {}

  async checkBodyId(
    productId: string,
    userId: string,
    quantity: number,
  ): Promise<void> {
    const user = await this.userRepository.findOne({
      where: { id: userId },
    });
    if (!user) {
      throw new NotFoundException({
        statusCode: HttpStatus.NOT_FOUND,
        message: 'User not found',
      });
    }

    const product = await this.productRepository.findOne({
      where: { id: productId },
    });
    if (!product || product.inventory < quantity) {
      throw new NotFoundException({
        statusCode: HttpStatus.NOT_FOUND,
        message: 'product not found or quantity is enough',
      });
    }
  }

  async addProduct(data: AddProductDto): Promise<AddProductResponse> {
    await this.checkBodyId(data.productId, data.userId, data.quantity);
    const product = await this.productRepository.findOne({
      where: { id: data.productId },
    });

    const cart = await this.cartRepository.findOne({
      where: {
        userId: data.userId,
        storeId: product.storeId,
      },
      relations: ['cartItems'],
    });

    if ((cart && cart.orderId != null) || !cart) {
      const cartNew = await this.cartRepository.create({
        userId: data.userId,
        storeId: product.storeId,
      });
      await this.cartRepository.save(cartNew);

      const cartItemNew = await this.cartItemRepository.create({
        cartId: cartNew.id,
        productId: data.productId,
        quantity: data.quantity,
      });

      await this.cartItemRepository.save(cartItemNew);

      return {
        Cart: cartNew,
        CartItem: cartItemNew,
      };
    }

    let cartItem = await this.cartItemRepository.findOne({
      where: { cartId: cart.id, productId: data.productId },
    });

    let newQuantity = data.quantity;
    if (cartItem) {
      newQuantity += cartItem.quantity;
      if (newQuantity > product.inventory) {
        throw new NotFoundException({
          statusCode: HttpStatus.BAD_REQUEST,
          message: 'Quantity is not enough',
        });
      }
      cartItem.quantity = newQuantity;
      await this.cartItemRepository.save(cartItem);
    } else {
      cartItem = await this.cartItemRepository.create({
        cartId: cart.id,
        productId: data.productId,
        quantity: newQuantity,
      });

      await this.cartItemRepository.save(cartItem);
    }

    return {
      Cart: cart,
      CartItem: cartItem,
    };
  }

  async update(id: string, data: UpdateCartDto): Promise<AddProductResponse> {
    await this.checkBodyId(data.productId, data.userId, data.quantity);
    const cart = await this.cartRepository.findOne({ where: { id: id } });
    if (!cart) {
      throw new NotFoundException({
        statusCode: HttpStatus.NOT_FOUND,
        message: 'cart not found',
      });
    }
    const cartItems = await this.cartItemRepository.find({
      where: { cartId: id },
    });

    if (data.quantity === 0 && cartItems.length === 0) {
      throw new BadRequestException({
        statusCode: HttpStatus.BAD_REQUEST,
        message: 'shopping cart is empty',
      });
    }
    if (data.quantity === 0 && cartItems.length !== 0) {
      await this.cartItemRepository.delete({
        cartId: id,
        productId: data.productId,
      });
      return {
        Cart: cart,
        CartItem: null,
      };
    }
    const cartItem = await this.cartItemRepository.findOne({
      where: { cartId: id, productId: data.productId },
    });
    cartItem.quantity = data.quantity;
    await this.cartItemRepository.save(cartItem);
    return {
      Cart: cart,
      CartItem: cartItem,
    };
  }

  async getDetail(id: string): Promise<CartGetDetailResponse> {
    const cart = await this.cartRepository.findOne({ where: { id: id } });
    if (!cart) {
      throw new NotFoundException({
        statusCode: HttpStatus.NOT_FOUND,
        message: 'cart not found',
      });
    }
    const cartItems = await this.cartItemRepository.find({
      where: { cartId: id },
    });
    return {
      Cart: cart,
      Items: cartItems,
    };
  }

  async delete(id: string): Promise<DeleteResponse<Cart>> {
    const cart = await this.cartRepository.findOne({ where: { id: id } });
    if (!cart) {
      throw new NotFoundException({
        statusCode: HttpStatus.NOT_FOUND,
        message: 'cart not found',
      });
    }
    await this.cartItemRepository.delete({ cartId: id });
    await this.cartRepository.delete({ id: id });
    return {
      message: 'delete product successful',
      data: cart,
    };
  }

  async getListItemByUser(userId: string): Promise<any> {
    const carts = await this.cartRepository.find({
      where: { userId: userId },
      relations: ['cartItems'],
    });
    return carts;
  }

  async getList(filter: CartFilterType): Promise<any> {
    const itemsPerPage: number = Number(filter.items_per_page) || 10;
    const page: number = filter.page || 1;
    const skip = page - 1 ? (page - 1) * itemsPerPage : 0;
    let sort_by: string = filter.sort_by || 'createdAt';
    const order_by = filter.order_by?.toLowerCase() === 'desc' ? 'DESC' : 'ASC';
    const userId: string = filter.userId || '';
    const orderId: string = filter.orderId || '';
    const isOrder: boolean = filter.isOrder;

    const query = this.cartRepository.createQueryBuilder('cart');

    if (userId || isOrder !== undefined || orderId) {
      query.andWhere(
        new Brackets((qb) => {
          if (isOrder !== undefined) {
            if (isOrder) {
              qb.andWhere('cart.orderId IS NULL ');
            } else {
              qb.andWhere('cart.orderId IS NOT NULL ');
            }
          }
          if (orderId) {
            qb.andWhere('cart.orderId = :orderId', { orderId });
          }
          if (userId) {
            qb.andWhere('cart.userId = :userId', { userId });
          }
        }),
      );
    }

    if (!SORT_BY_CART.includes(sort_by)) {
      sort_by = 'createdAt';
    }
    query.leftJoinAndSelect('cart.cartItems', 'cartItems');
    query.leftJoinAndSelect('cartItems.product', 'product');

    query.orderBy(`cart.${sort_by}`, order_by);
    query.skip(skip);
    query.take(itemsPerPage);

    const [data, total] = await query.getManyAndCount();

    return {
      data,
      total,
      currentPage: page,
      itemsPerPage,
    };
  }
}
