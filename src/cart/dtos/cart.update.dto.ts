import { AddProductDto } from './cart.add-product.dto';

export class UpdateCartDto extends AddProductDto {}
