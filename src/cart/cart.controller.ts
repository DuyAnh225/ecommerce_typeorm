import {
  Body,
  Controller,
  Param,
  Post,
  Put,
  Get,
  Delete,
  Query,
} from '@nestjs/common';
import { CartService } from './cart.service';
import { ApiTags } from '@nestjs/swagger';
import { Roles } from 'src/auth/decorators/role.decorator';
import { AddProductDto } from './dtos/cart.add-product.dto';
import { AddProductResponse } from './interfaces/cart.add-product-response.interface';
import { UpdateCartDto } from './dtos/cart.update.dto';
import { CartGetDetailResponse } from './interfaces/cart.get-detail-response.interface';
import { DeleteResponse } from 'src/common/helper/interfaces/helper.response-delete';
import { Cart } from 'src/common/database/entities/Cart';
import {
  CartCreateDoc,
  CartDeleteDoc,
  CartGetDetailDoc,
  CartUpdateDoc,
} from './docs/cart.doc';
import { CartFilterType } from './interfaces/cart.filter-type.interface';

@ApiTags('Cart')
@Controller('cart')
export class CartController {
  constructor(private readonly cartService: CartService) {}

  @CartCreateDoc()
  @Post()
  @Roles('admin', 'user', 'store')
  create(@Body() body: AddProductDto): Promise<AddProductResponse> {
    return this.cartService.addProduct(body);
  }

  @CartUpdateDoc()
  @Put(':id')
  @Roles('admin', 'user', 'store')
  update(
    @Param('id') id: string,
    @Body() body: UpdateCartDto,
  ): Promise<AddProductResponse> {
    return this.cartService.update(id, body);
  }

  @CartGetDetailDoc()
  @Get(':id')
  @Roles('admin', 'user', 'store')
  getDetail(@Param('id') id: string): Promise<CartGetDetailResponse> {
    return this.cartService.getDetail(id);
  }

  @CartDeleteDoc()
  @Delete(':id')
  @Roles('admin', 'store')
  delete(@Param('id') id: string): Promise<DeleteResponse<Cart>> {
    return this.cartService.delete(id);
  }

  @Get()
  @Roles('admin', 'user', 'store')
  getAll(@Query() filter: CartFilterType): Promise<any> {
    return this.cartService.getList(filter);
  }
}
