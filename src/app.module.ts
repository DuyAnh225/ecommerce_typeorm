import { Module, ValidationPipe } from '@nestjs/common';

import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { AuthModule } from './auth/auth.module';
import { APP_GUARD, APP_PIPE } from '@nestjs/core';
import { AuthGuard } from './auth/auth.guard';
import { RoleGuard } from './auth/role.guard';
import { JwtService } from '@nestjs/jwt';
import { User } from './common/database/entities/User';
import { Token } from './common/database/entities/Token';
import { Cart } from './common/database/entities/Cart';
import { CartItem } from './common/database/entities/CartItem';
import { Category } from './common/database/entities/Category';
import { Feedback } from './common/database/entities/Feedback';
import { Galery } from './common/database/entities/Galery';
import { Order } from './common/database/entities/Order';
import { Payment } from './common/database/entities/Payment';
import { Product } from './common/database/entities/Product';
import { Shipping } from './common/database/entities/Shipping';
import { Store } from './common/database/entities/Store';
import { UserModule } from './user/user.module';
import { StoreModule } from './store/store.module';
import { CategoryModule } from './category/category.module';
import { ProductModule } from './product/product.module';
import { FeedbackModule } from './feedback/feedback.module';
import { CartModule } from './cart/cart.module';
import { Wishlist } from './common/database/entities/WishList';
import { OrderModule } from './order/order.module';
import { WishlistModule } from './wishlist/wishlist.module';
import { MediaModule } from './media/media.module';
import { Media } from './common/database/entities/Media';
import { FeeShippingModule } from './fee-shipping/fee-shipping.module';
import { Address } from './common/database/entities/Address';
import { AddressModule } from './address/address.module';
import { PaymentModule } from './payment/payment.module';
import { ScheduleModule } from '@nestjs/schedule';
import { TaskModule } from './task/task.module';
import { FollowModule } from './follow/follow.module';
import { Follow } from './common/database/entities/Follow';

@Module({
  imports: [
    ConfigModule.forRoot(),
    TypeOrmModule.forRootAsync({
      imports: [TypeOrmModule.forFeature([User, Token]), ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        type: 'mysql',
        host: configService.get<string>('DB_HOST'),
        port: configService.get<number>('DB_PORT'),
        username: configService.get<string>('DB_USERNAME'),
        password: configService.get<string>('DB_PASSWORD'),
        database: configService.get<string>('DB_NAME'),
        entities: [
          User,
          Token,
          Cart,
          CartItem,
          Category,
          Feedback,
          Galery,
          Order,
          Payment,
          Product,
          Shipping,
          Store,
          Wishlist,
          Media,
          Address,
          Follow
        ],
        synchronize: true,
        connectTimeout: 30000,
        logging: true,
      }),
      inject: [ConfigService],
    }),
    AuthModule,
    TypeOrmModule.forFeature([User, Token]),
    UserModule,
    StoreModule,
    CategoryModule,
    ProductModule,
    FeedbackModule,
    CartModule,
    OrderModule,
    WishlistModule,
    MediaModule,
    FeeShippingModule,
    AddressModule,
    PaymentModule,
    ScheduleModule.forRoot(),
    TaskModule,
    FollowModule,
  ],
  providers: [
    ConfigService,
    JwtService,
    {
      provide: APP_PIPE,
      useClass: ValidationPipe,
    },
    {
      provide: APP_GUARD,
      useClass: AuthGuard,
    },
    {
      provide: APP_GUARD,
      useClass: RoleGuard,
    },
  ],
})
export class AppModule {}
