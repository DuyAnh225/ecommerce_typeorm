export interface WishlistFilterType {
  items_per_page?: number;
  page?: number;
  search?: string;
  sort_by?: string;
  order_by?: string;
  productId?: string;
  userId?: string;
}
