import { Wishlist } from "src/common/database/entities/WishList";


export interface WishlistPaginationResponseType {
  data: Wishlist[];
  total: number;
  currentPage: number;
  itemsPerPage: number;
}
