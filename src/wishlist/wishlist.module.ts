import { Module } from '@nestjs/common';
import { WishlistService } from './wishlist.service';
import { WishlistController } from './wishlist.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Product } from 'src/common/database/entities/Product';
import { User } from 'src/common/database/entities/User';
import { Wishlist } from 'src/common/database/entities/WishList';

@Module({
  imports: [TypeOrmModule.forFeature([Product, User, Wishlist])],
  controllers: [WishlistController],
  providers: [WishlistService],
})
export class WishlistModule {}
