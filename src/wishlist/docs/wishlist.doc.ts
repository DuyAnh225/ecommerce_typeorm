import { applyDecorators } from '@nestjs/common';
import { Wishlist } from '../../common/database/entities/WishList';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiParam,
  ApiQuery,
  ApiResponse,
} from '@nestjs/swagger';
import {
  WishlistCreateSerialization,
  WishlistGetDetailSerialization,
  WishlistGetListSerialization,
  WishlistUpdateSerialization,
} from '../serializations/wishlist.serialization';
import { faker } from '@faker-js/faker';

export function WishlistCreateDoc(): MethodDecorator {
  return applyDecorators(
    ApiBearerAuth(),
    ApiOperation({ summary: 'module wishlist' }),
    ApiResponse({
      status: 200,
      description: 'Success',
      type: WishlistCreateSerialization,
    }),
  );
}

export function WishlistUpdateDoc(): MethodDecorator {
  return applyDecorators(
    ApiBearerAuth(),
    ApiOperation({ summary: 'module wishlist' }),
    ApiParam({
      name: 'id',
      example: faker.string.uuid(),
      type: 'string',
    }),
    ApiResponse({
      status: 200,
      description: 'Success',
      type: WishlistUpdateSerialization,
    }),
  );
}

export function WishlistGetDetailDoc(): MethodDecorator {
  return applyDecorators(
    ApiBearerAuth(),
    ApiOperation({ summary: 'module wishlist' }),
    ApiParam({
      name: 'id',
      example: faker.string.uuid(),
      type: 'string',
    }),
    ApiResponse({
      status: 200,
      description: 'Success',
      type: WishlistGetDetailSerialization,
    }),
  );
}

export function WishlistDeleteDoc(): MethodDecorator {
  return applyDecorators(
    ApiBearerAuth(),
    ApiOperation({ summary: 'module wishlist' }),
    ApiParam({
      name: 'id',
      example: faker.string.uuid(),
      type: 'string',
    }),
    ApiResponse({
      status: 200,
      description: 'Success',
      type: WishlistCreateSerialization,
    }),
  );
}

export function WishlistGetListDoc(): MethodDecorator {
  return applyDecorators(
    ApiOperation({ summary: 'module wishlist' }),
    ApiQuery({
      name: 'items_per_page',
      example: faker.number.int(20),
      type: 'number',
    }),
    ApiBearerAuth(),
    ApiQuery({
      name: 'page',
      example: faker.number.int(20),
      type: 'number',
    }),
    ApiQuery({
      name: 'search',
      example: 'meat',
      type: 'string',
    }),
    ApiQuery({
      name: 'sort_by',
      example: 'createdAt',
      type: 'string',
    }),
    ApiQuery({
      name: 'order_by',
      example: 'desc',
      type: 'string',
    }),
    ApiQuery({
      name: 'productId',
      example: faker.string.uuid(),
      type: 'string',
    }),
    ApiQuery({
      name: 'userId',
      example: faker.string.uuid(),
      type: 'string',
    }),
    ApiResponse({
      status: 200,
      description: 'Success',
      type: WishlistGetListSerialization,
    }),
  );
}
