import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
} from '@nestjs/common';
import { WishlistService } from './wishlist.service';
import { Roles } from '../auth/decorators/role.decorator';
import { CreateWishListDto } from './dtos/wishlist.create.dto';
import { Wishlist } from 'src/common/database/entities/WishList';
import { UpdateWishListDto } from './dtos/wishlist.update.dto';
import { DeleteResponse } from 'src/common/helper/interfaces/helper.response-delete';
import { WishlistFilterType } from './interfaces/wishlist.filter-type.interface';
import { WishlistPaginationResponseType } from './interfaces/wishlist.pagination-response-type.interface';
import { ApiTags } from '@nestjs/swagger';
import {
  WishlistCreateDoc,
  WishlistDeleteDoc,
  WishlistGetDetailDoc,
  WishlistGetListDoc,
  WishlistUpdateDoc,
} from './docs/wishlist.doc';

@ApiTags('Wishlist')
@Controller('wishlist')
export class WishlistController {
  constructor(private readonly wishlistService: WishlistService) {}

  @WishlistCreateDoc()
  @Post()
  @Roles('admin', 'user', 'store')
  create(@Body() body: CreateWishListDto): Promise<Wishlist> {
    console.log('run api create wishlist');
    return this.wishlistService.create(body);
  }

  @WishlistUpdateDoc()
  @Put(':id')
  @Roles('admin', 'user', 'store')
  update(
    @Param('id') id: string,
    @Body() body: UpdateWishListDto,
  ): Promise<Wishlist> {
    return this.wishlistService.update(id, body);
  }

  @WishlistGetDetailDoc()
  @Get(':id')
  @Roles('admin', 'user', 'store')
  getDetail(@Param('id') id: string): Promise<Wishlist> {
    return this.wishlistService.getDetail(id);
  }

  @WishlistDeleteDoc()
  @Delete(':id')
  @Roles('admin', 'user', 'store')
  delete(@Param('id') id: string): Promise<DeleteResponse<Wishlist>> {
    return this.wishlistService.delete(id);
  }

  @WishlistGetListDoc()
  @Get()
  @Roles('admin', 'user', 'store')
  getAll(
    @Query() filter: WishlistFilterType,
  ): Promise<WishlistPaginationResponseType> {
    return this.wishlistService.getAll(filter);
  }
}
