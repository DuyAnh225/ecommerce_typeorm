import { CreateWishListDto } from './wishlist.create.dto';

export class UpdateWishListDto extends CreateWishListDto {}
