import { faker } from '@faker-js/faker';
import { ApiProperty } from '@nestjs/swagger';
import { Wishlist } from '../../common/database/entities/WishList';
import { WishlistPaginationResponseType } from '../interfaces/wishlist.pagination-response-type.interface';
import { DeleteResponse } from 'src/common/helper/interfaces/helper.response-delete';

export class WishlistCreateSerialization {
  @ApiProperty({
    example: faker.string.uuid(),
    required: true,
    nullable: false,
  })
  readonly id: string;

  @ApiProperty({
    example: faker.string.uuid(),
    required: true,
    nullable: false,
  })
  readonly userId: string;

  @ApiProperty({
    example: faker.string.uuid(),
    required: true,
    nullable: false,
  })
  readonly productId: string;

  @ApiProperty({
    example: faker.date.birthdate(),
    required: true,
    nullable: false,
  })
  readonly createdAt: Date;

  @ApiProperty({
    example: faker.date.birthdate(),
    required: true,
    nullable: false,
  })
  readonly updatedAt: Date;
}

export class WishlistUpdateSerialization extends WishlistCreateSerialization {}

export class WishlistGetListSerialization
  implements WishlistPaginationResponseType
{
  @ApiProperty({
    type: [WishlistUpdateSerialization],
    required: true,
    nullable: false,
  })
  data: Wishlist[];

  @ApiProperty({
    example: faker.number.int(10),
    required: true,
    nullable: false,
  })
  total: number;

  @ApiProperty({
    example: faker.number.int(10),
    required: true,
    nullable: false,
  })
  currentPage: number;

  @ApiProperty({
    example: faker.number.int(10),
    required: true,
    nullable: false,
  })
  itemsPerPage: number;
}

export class WishlistGetDetailSerialization extends WishlistUpdateSerialization {}

export class ProductDeleteSerialization implements DeleteResponse<Wishlist> {
  @ApiProperty({
    example: 'delete product successful',
  })
  message: string;
  @ApiProperty({
    type: [WishlistUpdateSerialization],
    required: true,
    nullable: false,
  })
  data: Wishlist;
}
