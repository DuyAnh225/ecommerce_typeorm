import {
  BadRequestException,
  HttpStatus,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Product } from 'src/common/database/entities/Product';
import { User } from 'src/common/database/entities/User';
import { Wishlist } from 'src/common/database/entities/WishList';
import { Brackets, Repository } from 'typeorm';
import { CreateWishListDto } from './dtos/wishlist.create.dto';
import { SORT_BY_WISHLIST } from './constants/wishlist.sort-by.constants';
import { WishlistFilterType } from './interfaces/wishlist.filter-type.interface';
import { WishlistPaginationResponseType } from './interfaces/wishlist.pagination-response-type.interface';
import { DeleteResponse } from 'src/common/helper/interfaces/helper.response-delete';
import { UpdateWishListDto } from './dtos/wishlist.update.dto';

@Injectable()
export class WishlistService {
  constructor(
    @InjectRepository(Product)
    private readonly productRepository: Repository<Product>,
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
    @InjectRepository(Wishlist)
    private readonly wishlistRepository: Repository<Wishlist>,
  ) {}

  async checkBodyId(userId: string, productId: string): Promise<void> {
    const product = await this.productRepository.findOne({
      where: { id: productId },
    });
    if (!product) {
      throw new NotFoundException({
        statusCode: HttpStatus.NOT_FOUND,
        message: 'product not found',
      });
    }

    const user = await this.userRepository.findOne({
      where: { id: userId },
    });
    if (!user) {
      throw new NotFoundException({
        statusCode: HttpStatus.NOT_FOUND,
        message: 'user not found',
      });
    }
  }

  async create(wishlistData: CreateWishListDto): Promise<Wishlist> {
    await this.checkBodyId(wishlistData.userId, wishlistData.productId);

    const wishlist = await this.wishlistRepository.findOne({
      where: { productId: wishlistData.productId, userId: wishlistData.userId },
    });

    if (wishlist) {
      throw new BadRequestException({
        statusCode: HttpStatus.BAD_REQUEST,
        message: 'The product has been added to wish list',
      });
    }

    const newWishList = await this.wishlistRepository.create({
      userId: wishlistData.userId,
      productId: wishlistData.productId,
    });

    return await this.wishlistRepository.save(newWishList);
  }
  async update(id: string, wishlistData: UpdateWishListDto): Promise<Wishlist> {
    await this.checkBodyId(wishlistData.userId, wishlistData.productId);

    const wishlist = await this.wishlistRepository.findOne({
      where: { id: id },
    });

    if (!wishlist) {
      throw new NotFoundException({
        statusCode: HttpStatus.NOT_FOUND,
        message: 'wishlist not found',
      });
    }

    const wishlistBody = await this.wishlistRepository.findOne({
      where: { productId: wishlistData.productId, userId: wishlistData.userId },
    });

    if (wishlistBody) {
      throw new BadRequestException({
        statusCode: HttpStatus.BAD_REQUEST,
        message: 'The product has been added to wish list',
      });
    }
    wishlist.productId = wishlistData.productId;
    wishlist.userId = wishlistData.userId;

    return await this.wishlistRepository.save(wishlist);
  }

  async getDetail(id: string): Promise<Wishlist> {
    const wishlist = await this.wishlistRepository.findOne({
      where: { id: id },
      relations: ['product'],
    });

    if (!wishlist) {
      throw new NotFoundException({
        statusCode: HttpStatus.NOT_FOUND,
        message: 'wishlist not found',
      });
    }
    return wishlist;
  }

  async delete(id: string): Promise<DeleteResponse<Wishlist>> {
    const wishlist = await this.wishlistRepository.findOne({
      where: { id: id },
    });
    if (!wishlist) {
      throw new NotFoundException({
        statusCode: HttpStatus.NOT_FOUND,
        message: 'wishlist not found',
      });
    }
    await this.wishlistRepository.delete({
      id: id,
    });
    return {
      message: 'delete wishlist successful',
      data: wishlist,
    };
  }

  async getAll(
    filter: WishlistFilterType,
  ): Promise<WishlistPaginationResponseType> {
    const itemsPerPage: number = Number(filter.items_per_page) || 10;
    const page: number = filter.page || 1;
    const search: string = filter.search || '';
    const skip = page - 1 ? (page - 1) * itemsPerPage : 0;
    let sort_by: string = filter.sort_by || 'createdAt';
    const order_by = filter.order_by?.toLowerCase() === 'desc' ? 'DESC' : 'ASC';

    const productId: string = filter.productId || '';
    const userId: string = filter.userId || '';

    const query = this.wishlistRepository.createQueryBuilder('wishlist');

    if (productId || userId) {
      query.andWhere(
        new Brackets((qb) => {
          if (productId) {
            qb.andWhere('wishlist.productId = :productId', { productId });
          }
          if (userId) {
            qb.andWhere('wishlist.userId = :userId', { userId });
          }
        }),
      );
    }

    if (!SORT_BY_WISHLIST.includes(sort_by)) {
      sort_by = 'createdAt';
    }

    query.orderBy(`wishlist.${sort_by}`, order_by);
    query.skip(skip);
    query.take(itemsPerPage);

    const [data, total] = await query.getManyAndCount();

    return {
      data,
      total,
      currentPage: page,
      itemsPerPage,
    };
  }
}
