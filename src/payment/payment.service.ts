import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import * as paypal from 'paypal-rest-sdk';

@Injectable()
export class PaymentService {
  private readonly clientId;
  private readonly clientSecret;
  constructor(private readonly configService: ConfigService) {
    this.clientId = this.configService.get('PAYPAL_CLIENT_ID');
    this.clientSecret = this.configService.get('PAYPAL_CLIENT_SECRET');
    paypal.configure({
      mode: 'sandbox', //'live'
      client_id: this.clientId,
      client_secret: this.clientSecret,
    });
  }

  async create(data: any): Promise<any> {
    


  }
  async createPayment() {}

  async executePayment(payerId: string, paymentId: string) {}
}
