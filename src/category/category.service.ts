import {
  BadRequestException,
  HttpStatus,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Category } from 'src/common/database/entities/Category';
import { Brackets, Like, Repository } from 'typeorm';
import { CreateCategoryDto } from './dtos/category.create.dto';
import { UpdateCategoryDto } from './dtos/category.update.dto';
import { CategoryFilterType } from './interfaces/category.filter-type.interface';
import { CategoryPaginationResponseType } from './interfaces/category.pagination-response-type.interface';
import { SORT_BY_CATEGORY } from './constants/category.sort-by.constant';
import { DeleteResponse } from 'src/common/helper/interfaces/helper.response-delete';

@Injectable()
export class CategoryService {
  constructor(
    @InjectRepository(Category)
    private readonly categoryRepository: Repository<Category>,
  ) {}

  async create(categoryData: CreateCategoryDto): Promise<Category> {
    const category = await this.categoryRepository.findOne({
      where: { name: categoryData.name },
    });
    if (category) {
      throw new BadRequestException({
        statusCode: HttpStatus.BAD_REQUEST,
        message: 'category is exist',
      });
    }
    const categoryNew = await this.categoryRepository.create({
      name: categoryData.name,
      description: categoryData.description,
    });

    return await this.categoryRepository.save(categoryNew);
  }

  async update(id: string, categoryData: UpdateCategoryDto): Promise<Category> {
    const category = await this.categoryRepository.findOne({
      where: [{ id: id }, { name: categoryData.name }],
    });

    if (!category) {
      throw new NotFoundException({
        statusCode: HttpStatus.NOT_FOUND,
        message: 'category does not exist or name category is exist',
      });
    }
    category.name = categoryData.name;
    category.description = categoryData.description;
    category.updatedAt = new Date();
    return await this.categoryRepository.save(category);
  }

  async getAll(
    filter: CategoryFilterType,
  ): Promise<CategoryPaginationResponseType> {
    const itemsPerPage: number = Number(filter.items_per_page) || 10;
    const page: number = filter.page || 1;
    const search: string = filter.search || '';
    const skip = page - 1 ? (page - 1) * itemsPerPage : 0;
    let sort_by: string = filter.sort_by || 'created_at';
    const order_by = filter.order_by?.toLowerCase() === 'desc' ? 'DESC' : 'ASC';

    if (!SORT_BY_CATEGORY.includes(sort_by)) {
      sort_by = 'createdAt';
    }

    const query = this.categoryRepository.createQueryBuilder('category');

    if (search) {
      query.andWhere(
        new Brackets((qb) => {
          qb.where('category.name LIKE :search', {
            search: `%${search}%`,
          }).orWhere('category.description LIKE :search', {
            search: `%${search}%`,
          });
        }),
      );
    }

    query.orderBy(`category.${sort_by}`, order_by);
    query.skip(skip);
    query.take(itemsPerPage);

    const [data, total] = await query.getManyAndCount();

    return {
      data,
      total,
      currentPage: page,
      itemsPerPage,
    };
  }

  async getDetail(id: string): Promise<Category> {
    const category = await this.categoryRepository.findOne({
      where: { id: id },
    });
    if (!category) {
      throw new BadRequestException({
        statusCode: HttpStatus.NOT_FOUND,
        message: 'Category not found',
      });
    }
    return category;
  }

  async delete(id: string): Promise<DeleteResponse<Category>> {
    const category = await this.categoryRepository.findOne({
      where: { id: id },
    });

    if (!category) {
      throw new BadRequestException({
        statusCode: HttpStatus.NOT_FOUND,
        message: 'Category not found',
      });
    }
    await this.categoryRepository.delete({
      id: id,
    });
    return {
      message: 'delete category successful',
      data: category,
    };
  }

  /**
   *  
   

  

  
   */
}
