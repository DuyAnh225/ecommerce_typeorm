import { applyDecorators } from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiParam,
  ApiQuery,
  ApiResponse,
} from '@nestjs/swagger';
import {
  CategoryCreateSerialization,
  CategoryDeleteSerialization,
  CategoryGetDetailSerialization,
  CategoryGetListSerialization,
  CategoryUpdateSerialization,
} from '../serializations/category.serialization';
import { faker } from '@faker-js/faker';

export function CategoryCreateDoc(): MethodDecorator {
  return applyDecorators(
    ApiBearerAuth(),
    ApiOperation({ summary: 'module category' }),
    ApiResponse({
      status: 200,
      description: 'Success',
      type: CategoryCreateSerialization,
    }),
  );
}

export function CategoryUpdateDoc(): MethodDecorator {
  return applyDecorators(
    ApiBearerAuth(),
    ApiOperation({ summary: 'module category' }),
    ApiParam({
      name: 'id',
      example: faker.string.uuid(),
      type: 'string',
    }),
    ApiResponse({
      status: 200,
      description: 'Success',
      type: CategoryUpdateSerialization,
    }),
  );
}

export function CategoryGetListDoc(): MethodDecorator {
  return applyDecorators(
    ApiBearerAuth(),
    ApiOperation({ summary: 'module category' }),
    ApiQuery({
      name: 'items_per_page',
      example: faker.number.int(20),
      type: 'number',
    }),
    ApiQuery({
      name: 'page',
      example: faker.number.int(20),
      type: 'number',
    }),
    ApiQuery({
      name: 'search',
      example: 'meat',
      type: 'string',
    }),
    ApiResponse({
      status: 200,
      description: 'Success',
      type: CategoryGetListSerialization,
    }),
  );
}

export function CategoryGetDetailDoc(): MethodDecorator {
  return applyDecorators(
    ApiBearerAuth(),
    ApiOperation({ summary: 'module category' }),
    ApiParam({
      name: 'id',
      example: faker.string.uuid(),
      type: 'string',
    }),
    ApiResponse({
      status: 200,
      description: 'Success',
      type: CategoryGetDetailSerialization,
    }),
  );
}

export function CategoryDeleteDoc(): MethodDecorator {
  return applyDecorators(
    ApiBearerAuth(),
    ApiOperation({ summary: 'module category' }),
    ApiParam({
      name: 'id',
      example: faker.string.uuid(),
      type: 'string',
    }),
    ApiResponse({
      status: 200,
      description: 'Success',
      type: CategoryDeleteSerialization,
    }),
  );
}
