import {
  Body,
  Controller,
  Param,
  Post,
  Put,
  Get,
  Query,
  Delete,
} from '@nestjs/common';
import { CategoryService } from './category.service';
import { ApiTags } from '@nestjs/swagger';
import { Roles } from 'src/auth/decorators/role.decorator';
import { CreateCategoryDto } from './dtos/category.create.dto';
import { Category } from 'src/common/database/entities/Category';
import { UpdateCategoryDto } from './dtos/category.update.dto';
import { CategoryFilterType } from './interfaces/category.filter-type.interface';
import { CategoryPaginationResponseType } from './interfaces/category.pagination-response-type.interface';
import { DeleteResponse } from 'src/common/helper/interfaces/helper.response-delete';
import {
  CategoryCreateDoc,
  CategoryDeleteDoc,
  CategoryGetDetailDoc,
  CategoryGetListDoc,
  CategoryUpdateDoc,
} from './docs/category.doc';

@ApiTags('Category')
@Controller('category')
export class CategoryController {
  constructor(private readonly categoryService: CategoryService) {}

  @CategoryCreateDoc()
  @Post()
  @Roles('admin', 'store')
  create(@Body() body: CreateCategoryDto): Promise<Category> {
    return this.categoryService.create(body);
  }

  @CategoryUpdateDoc()
  @Put(':id')
  @Roles('admin', 'store')
  update(
    @Param('id') id: string,
    @Body() body: UpdateCategoryDto,
  ): Promise<Category> {
    return this.categoryService.update(id, body);
  }

  @CategoryGetListDoc()
  @Get()
  @Roles('admin', 'user', 'store')
  getList(
    @Query() filter: CategoryFilterType,
  ): Promise<CategoryPaginationResponseType> {
    return this.categoryService.getAll(filter);
  }

  @CategoryGetDetailDoc()
  @Roles('admin', 'user', 'store')
  @Get(':id')
  getDetail(@Param('id') id: string): Promise<Category> {
    return this.categoryService.getDetail(id);
  }

  @CategoryDeleteDoc()
  @Roles('admin')
  @Delete(':id')
  delete(@Param('id') id: string): Promise<DeleteResponse<Category>> {
    return this.categoryService.delete(id);
  }
}
