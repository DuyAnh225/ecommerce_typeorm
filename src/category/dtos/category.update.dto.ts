import { CreateCategoryDto } from './category.create.dto';

export class UpdateCategoryDto extends CreateCategoryDto {}
