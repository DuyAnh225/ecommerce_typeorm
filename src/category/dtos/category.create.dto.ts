import { faker } from '@faker-js/faker';
import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsNotEmpty, IsOptional, MinLength } from 'class-validator';

export class CreateCategoryDto {
  @ApiProperty({
    example: faker.commerce.product(),
    required: true,
  })
  @IsNotEmpty()
  @MinLength(1)
  @Type(() => String)
  name: string;
  @ApiProperty({
    example: faker.commerce.productDescription(),
    required: false,
  })
  @IsOptional()
  @Type(() => String)
  description?: string;
}
