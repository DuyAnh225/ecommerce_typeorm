import { Category } from 'src/common/database/entities/Category';

export interface CategoryPaginationResponseType {
  data: Category[];
  total: number;
  currentPage: number;
  itemsPerPage: number;
}
