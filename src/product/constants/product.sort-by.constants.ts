export const SORT_BY_PRODUCT: string[] = [
  'createdAt',
  'title',
  'price',
  'discountValue',
];
