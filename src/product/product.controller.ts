import {
  Body,
  Controller,
  Param,
  Post,
  Put,
  Get,
  Query,
  Delete,
  UploadedFiles,
} from '@nestjs/common';
import { ProductService } from './product.service';
import {  ApiTags } from '@nestjs/swagger';
import { Roles } from 'src/auth/decorators/role.decorator';
import { CreateProductDto } from './dtos/product.create.dto';
import { Product } from 'src/common/database/entities/Product';
import { UpdateProductDto } from './dtos/product.update.dto';
import { ProductFilterType } from './interfaces/product.filter-type.interface';
import { ProductPaginationResponseType } from './interfaces/product.pagination-response-type.interface';
import { DeleteResponse } from 'src/common/helper/interfaces/helper.response-delete';
import { UseInterceptors } from '@nestjs/common';
import {
  ProductCreateDoc,
  ProductDeleteDoc,
  ProductGetDetailDoc,
  ProductGetListDoc,
  ProductUpdateDoc,
} from './docs/product.doc';
import { FileInterceptor, FilesInterceptor } from '@nestjs/platform-express';

@ApiTags('Product')
@Controller('product')
export class ProductController {
  constructor(private readonly productService: ProductService) {}

  @Post('uploads/:id')
  @UseInterceptors(FilesInterceptor('files'))
  upload(
    @Param('id') id: string,
    @UploadedFiles() files: Express.Multer.File[],
  ): Promise<Product> {
    return this.productService.uploadMedias(id, files);
  }

  @ProductCreateDoc()
  @Post()
  @Roles('store', 'admin')
  create(@Body() body: CreateProductDto): Promise<Product> {
    return this.productService.create(body);
  }

  @ProductUpdateDoc()
  @Put(':id')
  @Roles('store', 'admin')
  update(
    @Param('id') id: string,
    @Body() body: UpdateProductDto,
  ): Promise<Product> {
    return this.productService.update(id, body);
  }

  @ProductGetListDoc()
  @Get()
  @Roles('admin', 'store', 'admin')
  getAll(
    @Query() filter: ProductFilterType,
  ): Promise<ProductPaginationResponseType> {
    return this.productService.getList(filter);
  }

  @ProductGetDetailDoc()
  @Get(':id')
  @Roles('admin', 'user', 'store')
  getDetail(@Param('id') id: string): Promise<Product> {
    return this.productService.getDetail(id);
  }

  @ProductDeleteDoc()
  @Delete(':id')
  @Roles('admin', 'store')
  delete(@Param('id') id: string): Promise<DeleteResponse<Product>> {
    return this.productService.delete(id);
  }
}
