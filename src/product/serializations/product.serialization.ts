import { ApiProperty } from '@nestjs/swagger';
import { faker } from '@faker-js/faker';
import { ProductPaginationResponseType } from '../interfaces/product.pagination-response-type.interface';
import { DeleteResponse } from 'src/common/helper/interfaces/helper.response-delete';
import { Product } from 'src/common/database/entities/Product';
import { Media } from 'src/common/database/entities/Media';

export class MediaSerialization {
  @ApiProperty({
    example: faker.string.uuid(),
    required: true,
    nullable: false,
  })
  id: string;

  @ApiProperty({
    example: faker.string.uuid(),
    required: true,
    nullable: false,
  })
  productId: string;

  @ApiProperty({
    example: faker.image.url(),
    required: true,
    nullable: false,
  })
  location: string;

  @ApiProperty({
    example: faker.date.birthdate(),
    required: true,
    nullable: false,
  })
  createdAt: string;

  @ApiProperty({
    example: faker.date.birthdate(),
    required: true,
    nullable: false,
  })
  updatedAt: string;
}
export class ProductCreateSerialization {
  @ApiProperty({
    example: faker.string.uuid(),
    required: true,
    nullable: false,
  })
  readonly id: string;
  @ApiProperty({
    example: faker.string.uuid(),
    required: true,
    nullable: false,
  })
  readonly categoryId: string;
  @ApiProperty({
    example: faker.string.uuid(),
    required: true,
    nullable: false,
  })
  readonly storeId: string;
  @ApiProperty({
    example: faker.commerce.productName(),
    required: true,
    nullable: false,
  })
  readonly title: string;
  @ApiProperty({
    example: faker.commerce.price(),
    required: true,
    nullable: false,
  })
  readonly price: number;
  @ApiProperty({
    example: 'sales',
    required: true,
    nullable: false,
  })
  readonly priceType: string;
  @ApiProperty({
    example: faker.commerce.price(),
    required: true,
    nullable: false,
  })
  readonly offerPrice: number;
  @ApiProperty({
    example: faker.internet.url(),
    required: true,
    nullable: false,
  })
  readonly thumbnail: string;
  @ApiProperty({
    example: faker.commerce.productDescription(),
    required: true,
    nullable: false,
  })
  readonly description: string;

  @ApiProperty({
    example: faker.commerce.productDescription(),
    required: true,
    nullable: false,
  })
  readonly additionalDetail: string;

  @ApiProperty({
    type: [MediaSerialization],
    required: true,
    nullable: false,
  })
  medias: Media[];

  @ApiProperty({
    example: faker.date.birthdate(),
    required: true,
    nullable: false,
  })
  readonly createdAt: Date;
  @ApiProperty({
    example: null,
    required: true,
    nullable: false,
  })
  readonly updatedAt: Date;
  @ApiProperty({
    example: null,
    required: true,
    nullable: false,
  })
  readonly deleted: number;
}

export class ProductUpdateSerialization {
  @ApiProperty({
    example: faker.string.uuid(),
    required: true,
    nullable: false,
  })
  readonly id: string;
  @ApiProperty({
    example: faker.string.uuid(),
    required: true,
    nullable: false,
  })
  readonly category_id: string;
  @ApiProperty({
    example: faker.string.uuid(),
    required: true,
    nullable: false,
  })
  readonly store_id: string;
  @ApiProperty({
    example: faker.commerce.productName(),
    required: true,
    nullable: false,
  })
  readonly title: string;
  @ApiProperty({
    example: faker.commerce.price(),
    required: true,
    nullable: false,
  })
  readonly price: number;
  @ApiProperty({
    example: 'sales',
    required: true,
    nullable: false,
  })
  readonly discountType: string;
  @ApiProperty({
    example: faker.commerce.price(),
    required: true,
    nullable: false,
  })
  readonly discountValue: number;
  @ApiProperty({
    example: faker.internet.url(),
    required: true,
    nullable: false,
  })
  readonly thumbnail: string;
  @ApiProperty({
    example: faker.commerce.productDescription(),
    required: true,
    nullable: false,
  })
  readonly description: string;
  @ApiProperty({
    example: faker.date.birthdate(),
    required: true,
    nullable: false,
  })
  readonly createdAt: Date;
  @ApiProperty({
    example: faker.date.birthdate(),
    required: true,
    nullable: false,
  })
  readonly updatedAt: Date;
  @ApiProperty({
    example: null,
    required: true,
    nullable: false,
  })
  readonly deleted: number;
}

export class ProductGetListSerialization
  implements ProductPaginationResponseType
{
  @ApiProperty({
    type: [ProductUpdateSerialization],
    required: true,
    nullable: false,
  })
  data: Product[];
  @ApiProperty({
    example: faker.number.int(10),
    required: true,
    nullable: false,
  })
  total: number;
  @ApiProperty({
    example: faker.number.int(10),
    required: true,
    nullable: false,
  })
  currentPage: number;
  @ApiProperty({
    example: faker.number.int(10),
    required: true,
    nullable: false,
  })
  itemsPerPage: number;
}

export class ProductGetDetailSerialization extends ProductUpdateSerialization {}

export class ProductDeleteSerialization implements DeleteResponse<Product> {
  @ApiProperty({
    example: 'delete product successful',
  })
  message: string;
  @ApiProperty({
    type: [ProductUpdateSerialization],
    required: true,
    nullable: false,
  })
  data: Product;
}

export class ProductUploadImgaesSerialization extends ProductCreateSerialization {}
