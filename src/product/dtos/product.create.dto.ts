import { faker } from '@faker-js/faker';
import { ApiProperty } from '@nestjs/swagger';

import {
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
  Min,
  min,
} from 'class-validator';
import { Type } from 'class-transformer';

export class CreateProductDto {
  @ApiProperty({
    example: faker.commerce.productName(),
    required: true,
  })
  @IsNotEmpty()
  @Type(() => String)
  title: string;

  @ApiProperty({
    example: faker.number.int(100),
    required: true,
  })
  @IsNotEmpty()
  @IsNumber()
  @Min(1)
  inventory: number;

  @ApiProperty({
    example: faker.commerce.price(),
    required: true,
  })
  @IsNotEmpty()
  @IsNumber()
  @Min(1000)
  price: number;

  @ApiProperty({
    example: faker.number.int(1000),
    required: true,
  })
  @IsNotEmpty()
  @IsNumber()
  @Min(10)
  weight: number;

  @ApiProperty({
    example: faker.string.uuid(),
    required: true,
  })
  @IsNotEmpty()
  @IsString()
  categoryId: string;
  @ApiProperty({
    example: faker.string.uuid(),
    required: true,
  })
  @IsNotEmpty()
  @IsString()
  storeId: string;

  @ApiProperty({
    example: faker.commerce.price(),
    required: false,
  })
  @IsOptional()
  @IsNumber()
  @Min(1000)
  offerPrice?: number;
  @ApiProperty({
    example: faker.internet.url(),
    required: false,
  })
  @IsOptional()
  @Type(() => String)
  thumbnail?: string;
  @ApiProperty({
    example: faker.commerce.productDescription(),
    required: false,
  })
  @IsOptional()
  @Type(() => String)
  description?: string;

  @ApiProperty({
    example: faker.location.streetAddress(),
    required: false,
  })
  @IsOptional()
  @Type(() => String)
  locationDetail?: string;

  @ApiProperty({
    example: 'fixed',
    required: false,
  })
  @IsOptional()
  @Type(() => String)
  priceType?: string;

  @ApiProperty({
    example: faker.commerce.productDescription(),
    required: false,
  })
  @IsOptional()
  @Type(() => String)
  additionalDetail?: string;
}
