import {
  BadRequestException,
  HttpStatus,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Product } from 'src/common/database/entities/Product';
import { Brackets, FindManyOptions, Like, Repository } from 'typeorm';
import { CreateProductDto } from './dtos/product.create.dto';
import { Category } from 'src/common/database/entities/Category';
import { Store } from 'src/common/database/entities/Store';
import { ProductFilterType } from './interfaces/product.filter-type.interface';
import { ProductPaginationResponseType } from './interfaces/product.pagination-response-type.interface';
import { SORT_BY_PRODUCT } from './constants/product.sort-by.constants';
import { DeleteResponse } from 'src/common/helper/interfaces/helper.response-delete';
import { UpdateProductDto } from './dtos/product.update.dto';
import { Media } from 'src/common/database/entities/Media';
import { MediaService } from 'src/media/media.service';

@Injectable()
export class ProductService {
  constructor(
    @InjectRepository(Product)
    private readonly productRepository: Repository<Product>,
    @InjectRepository(Category)
    private readonly categoryRepository: Repository<Category>,
    @InjectRepository(Store)
    private readonly storeRepository: Repository<Store>,
    @InjectRepository(Media)
    private readonly mediaRepository: Repository<Media>,
    private readonly mediaService: MediaService,
  ) {}

  async create(productData: CreateProductDto): Promise<Product> {
    const store = await this.storeRepository.findOne({
      where: { id: productData.storeId },
    });
    if (!store) {
      throw new NotFoundException({
        statusCode: HttpStatus.NOT_FOUND,
        message: 'Store not found',
      });
    }
    const category = await this.categoryRepository.findOne({
      where: { id: productData.categoryId },
    });
    if (!category) {
      throw new NotFoundException({
        statusCode: HttpStatus.NOT_FOUND,
        message: 'category not found',
      });
    }

    const product = await this.productRepository.findOne({
      where: {
        title: productData.title,
        categoryId: productData.categoryId,
        storeId: productData.storeId,
      },
    });
    if (product) {
      throw new BadRequestException({
        statusCode: HttpStatus.BAD_REQUEST,
        message: 'product is exist',
      });
    }
    const newProduct = await this.productRepository.create({
      categoryId: productData.categoryId,
      storeId: productData.storeId,
      title: productData.title,
      inventory: productData.inventory,
      price: productData.price,
      offerPrice: productData.offerPrice,
      thumbnail: productData.thumbnail,
      description: productData.description,
      locationDetail: productData.locationDetail,
      priceType: productData.priceType,
      additionalDetail: productData.additionalDetail,
      weight: productData.weight,
    });

    return await this.productRepository.save(newProduct);
  }

  async update(id: string, productData: UpdateProductDto): Promise<Product> {
    const product = await this.productRepository.findOne({
      where: { id: id },
    });
    if (!product) {
      throw new NotFoundException({
        statusCode: HttpStatus.NOT_FOUND,
        message: 'product does not exist',
      });
    }
    const store = await this.storeRepository.findOne({
      where: { id: productData.storeId },
    });
    if (!store) {
      throw new NotFoundException({
        statusCode: HttpStatus.NOT_FOUND,
        message: 'Store not found',
      });
    }
    const category = await this.categoryRepository.findOne({
      where: { id: productData.categoryId },
    });
    if (!category) {
      throw new NotFoundException({
        statusCode: HttpStatus.NOT_FOUND,
        message: 'category not found',
      });
    }

    product.categoryId = productData.categoryId;
    product.storeId = productData.storeId;
    product.title = productData.title;
    product.inventory = productData.inventory;
    product.price = productData.price;
    product.offerPrice = productData.offerPrice;
    product.thumbnail = productData.thumbnail;
    product.description = productData.description;
    product.locationDetail = productData.locationDetail;
    product.priceType = productData.priceType;
    product.additionalDetail = productData.additionalDetail;
    product.weight = productData.weight;
    product.updatedAt = new Date();

    return await this.productRepository.save(product);
  }

  async getList(
    filter: ProductFilterType,
  ): Promise<ProductPaginationResponseType> {
    const itemsPerPage: number = Number(filter.items_per_page) || 10;
    const page: number = filter.page || 1;
    const search: string = filter.search || '';
    const skip = page > 1 ? (page - 1) * itemsPerPage : 0;
    let sort_by: string = filter.sort_by || 'createdAt';
    const order_by = filter.order_by?.toLowerCase() === 'desc' ? 'DESC' : 'ASC';

    const storeId: string = filter.storeId || '';
    const categoryId: string = filter.categoryId || '';

    const query = this.productRepository.createQueryBuilder('product');

    query.leftJoinAndSelect('product.medias', 'medias');

    if (storeId || categoryId) {
      query.andWhere(
        new Brackets((qb) => {
          if (storeId) {
            qb.andWhere('product.storeId = :storeId', { storeId });
          }
          if (categoryId) {
            qb.andWhere('product.categoryId = :categoryId', { categoryId });
          }
        }),
      );
    }

    if (search) {
      query.andWhere(
        new Brackets((qb) => {
          qb.where('product.title LIKE :search', { search: `%${search}%` })
            .orWhere('product.description LIKE :search', {
              search: `%${search}%`,
            })
            .orWhere('product.discountType LIKE :search', {
              search: `%${search}%`,
            });
        }),
      );
    }

    if (!SORT_BY_PRODUCT.includes(sort_by)) {
      sort_by = 'createdAt';
    }

    query.orderBy(`product.${sort_by}`, order_by);
    query.skip(skip);
    query.take(itemsPerPage);

    const [data, total] = await query.getManyAndCount();

    return {
      data,
      total,
      currentPage: page,
      itemsPerPage,
    };
  }

  async getDetail(id: string): Promise<Product> {
    const product = await this.productRepository.findOne({
      where: { id: id },
      relations: ['category', 'medias'],
    });
    if (!product) {
      throw new NotFoundException({
        statusCode: HttpStatus.NOT_FOUND,
        message: 'product not found',
      });
    }
    return product;
  }

  async delete(id: string): Promise<DeleteResponse<Product>> {
    const product = await this.productRepository.findOne({
      where: { id: id },
    });
    if (!product) {
      throw new NotFoundException({
        statusCode: HttpStatus.NOT_FOUND,
        message: 'product not found',
      });
    }
    await this.productRepository.delete({
      id: id,
    });
    return {
      message: 'delete product successful',
      data: product,
    };
  }

  async checkProductId(productId: string): Promise<void> {}

  private maxNumberOfFiles = 4;

  async uploadMedias(
    productId: string,
    files: Express.Multer.File[],
  ): Promise<any> {
    const product = await this.productRepository.findOne({
      where: { id: productId },
    });
    if (!product) {
      throw new NotFoundException({
        statusCode: HttpStatus.NOT_FOUND,
        message: 'product not found',
      });
    }

    if (files.length > this.maxNumberOfFiles) {
      throw new BadRequestException({
        statusCode: HttpStatus.BAD_REQUEST,
        message: 'error: The maximum number of uploaded photos is 4',
      });
    }

    const existingMedias = await this.mediaRepository.find({
      where: { productId: productId },
    });

    const totalImages = existingMedias.length + files.length;

    if (
      totalImages >= this.maxNumberOfFiles ||
      existingMedias.length >= this.maxNumberOfFiles
    ) {
      for (const media of existingMedias) {
        await this.mediaRepository.delete({ id: media.id });
      }
    }
    for (const file of files) {
      const location = await this.mediaService.upload(file);
      const media = new Media();
      media.productId = productId;
      media.location = location;
      await this.mediaRepository.save(media);
    }
    product.medias = await this.mediaRepository.find({
      where: { productId: productId },
    });
    return product;
  }
}
