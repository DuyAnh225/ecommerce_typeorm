import { Module } from '@nestjs/common';
import { ProductService } from './product.service';
import { ProductController } from './product.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Product } from 'src/common/database/entities/Product';
import { Store } from 'src/common/database/entities/Store';
import { Category } from 'src/common/database/entities/Category';
import { MediaService } from 'src/media/media.service';
import { ConfigService } from '@nestjs/config';
import { Media } from 'src/common/database/entities/Media';

@Module({
  imports: [TypeOrmModule.forFeature([Product, Store, Category, Media])],
  controllers: [ProductController],
  providers: [ProductService, MediaService, ConfigService],
})
export class ProductModule {}
