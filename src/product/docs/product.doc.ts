import { applyDecorators } from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiParam,
  ApiQuery,
  ApiResponse,
} from '@nestjs/swagger';
import {
  ProductCreateSerialization,
  ProductDeleteSerialization,
  ProductGetDetailSerialization,
  ProductGetListSerialization,
  ProductUpdateSerialization,
} from '../serializations/product.serialization';
import { faker } from '@faker-js/faker';

export function ProductCreateDoc(): MethodDecorator {
  return applyDecorators(
    ApiBearerAuth(),
    ApiOperation({ summary: 'module product' }),
    ApiResponse({
      status: 200,
      description: 'Success',
      type: ProductCreateSerialization,
    }),
  );
}

export function ProductUpdateDoc(): MethodDecorator {
  return applyDecorators(
    ApiBearerAuth(),
    ApiOperation({ summary: 'module product' }),
    ApiParam({
      name: 'id',
      example: faker.string.uuid(),
      type: 'string',
    }),
    ApiResponse({
      status: 200,
      description: 'Success',
      type: ProductUpdateSerialization,
    }),
  );
}

export function ProductGetListDoc(): MethodDecorator {
  return applyDecorators(
    ApiOperation({ summary: 'module product' }),
    ApiQuery({
      name: 'items_per_page',
      example: faker.number.int(20),
      type: 'number',
    }),
    ApiBearerAuth(),
    ApiQuery({
      name: 'page',
      example: faker.number.int(20),
      type: 'number',
    }),
    ApiQuery({
      name: 'search',
      example: 'meat',
      type: 'string',
    }),
    ApiResponse({
      status: 200,
      description: 'Success',
      type: ProductGetListSerialization,
    }),
  );
}

export function ProductGetDetailDoc(): MethodDecorator {
  return applyDecorators(
    ApiBearerAuth(),
    ApiOperation({ summary: 'module product' }),
    ApiParam({
      name: 'id',
      example: faker.string.uuid(),
      type: 'string',
    }),
    ApiResponse({
      status: 200,
      description: 'Success',
      type: ProductGetDetailSerialization,
    }),
  );
}

export function ProductDeleteDoc(): MethodDecorator {
  return applyDecorators(
    ApiBearerAuth(),
    ApiOperation({ summary: 'module product' }),
    ApiParam({
      name: 'id',
      example: faker.string.uuid(),
      type: 'string',
    }),
    ApiResponse({
      status: 200,
      description: 'Success',
      type: ProductDeleteSerialization,
    }),
  );
}
