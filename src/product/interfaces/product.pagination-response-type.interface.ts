import { Product } from 'src/common/database/entities/Product';

export interface ProductPaginationResponseType {
  data: Product[];
  total: number;
  currentPage: number;
  itemsPerPage: number;
}
