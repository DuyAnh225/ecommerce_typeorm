import { Module } from '@nestjs/common';
import { OrderService } from './order.service';
import { OrderController } from './order.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Cart } from 'src/common/database/entities/Cart';
import { User } from 'src/common/database/entities/User';
import { Order } from 'src/common/database/entities/Order';
import { Shipping } from 'src/common/database/entities/Shipping';
import { Address } from 'src/common/database/entities/Address';
import { FeeShippingService } from 'src/fee-shipping/fee-shipping.service';
import { ConfigService } from '@nestjs/config';
import { Store } from 'src/common/database/entities/Store';

@Module({
  imports: [
    TypeOrmModule.forFeature([Cart, User, Order, Shipping, Address, Store]),
  ],
  controllers: [OrderController],
  providers: [OrderService, FeeShippingService, ConfigService],
})
export class OrderModule {}
