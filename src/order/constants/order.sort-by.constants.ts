export const SORT_BY_ORDER: string[] = [
  'createdAt',
  'fullname',
  'email',
  'phoneNumber',
  'address',
  'note',
  'totalMoney',
  'updatedAt',
];
