export enum OrderStatus {
  // Trạng thái mới tạo
  ReadyToPick = 'ready_to_pick',
  // Nhân viên đang lấy hàng
  Picking = 'picking',
  // Đã hủy đơn hàng
  Cancel = 'cancel',
  // Đang thu tiền người gửi
  MoneyCollectPicking = 'money_collect_picking',
  // Nhân viên đã lấy hàng
  Picked = 'picked',
  // Hàng đang nằm ở kho
  Storing = 'storing',
  // Đang luân chuyển hàng
  Transporting = 'transporting',
  // Đang phân loại hàng hóa
  Sorting = 'sorting',
  // Nhân viên đang giao cho người nhận
  Delivering = 'delivering',
  // Đang thu tiền người nhận
  MoneyCollectDelivering = 'money_collect_delivering',
  // Nhân viên đã giao hàng thành công
  Delivered = 'delivered',
  // Nhân viên giao hàng thất bại
  DeliveryFail = 'delivery_fail',
  // Đang đợi trả hàng về cho người gửi
  WaitingToReturn = 'waiting_to_return',
  // Trả hàng
  Return = 'return',
  // Đang luân chuyển hàng trả
  ReturnTransporting = 'return_transporting',
  // Đang phân loại hàng trả
  ReturnSorting = 'return_sorting',
  // Nhân viên đang đi trả hàng
  Returning = 'returning',
  // Nhân viên trả hàng thất bại
  ReturnFail = 'return_fail',
  // Nhân viên trả hàng thành công
  Returned = 'returned',
  // Đơn hàng ngoại lệ không nằm trong quy trình
  Exception = 'exception',
  // Hàng bị hư hỏng
  Damage = 'damage',
  // Hàng bị mất
  Lost = 'lost',
}
