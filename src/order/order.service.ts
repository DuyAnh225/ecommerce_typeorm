import {
  BadRequestException,
  HttpStatus,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Cart } from 'src/common/database/entities/Cart';
import { Order } from 'src/common/database/entities/Order';

import { User } from 'src/common/database/entities/User';
import { Brackets, Repository } from 'typeorm';
import { CreateOrderDto } from './dtos/order.create.dto';
import { OrderFilterType } from './interfaces/order.filter-type.interface';
import { OrderPaginationResponseType } from './interfaces/order.pagination-response-type.interface';
import { SORT_BY_ORDER } from './constants/order.sort-by.constants';
import { Address } from 'src/common/database/entities/Address';
import { FeeShippingService } from 'src/fee-shipping/fee-shipping.service';
import { Store } from 'src/common/database/entities/Store';
import { ConfigService } from '@nestjs/config';
import axios, { AxiosRequestConfig } from 'axios';
import { removeDiacriticsAndSpaces } from 'src/common/helper/removeDiacriticsAndSpaces';
import { GHNResponseData } from './interfaces/order.GHN-response-data';
import { OrderStatusCancel } from './constants/order.status-cancel.constant';

@Injectable()
export class OrderService {
  private readonly url_api_create_order;
  private readonly url_api_cancel_order;
  private readonly shop_id_dev;
  private readonly api_key_dev;
  private readonly config: AxiosRequestConfig;
  constructor(
    @InjectRepository(Cart) private readonly cartRepository: Repository<Cart>,
    @InjectRepository(User) private readonly userRepository: Repository<User>,
    @InjectRepository(Order)
    private readonly orderRepository: Repository<Order>,
    @InjectRepository(Address)
    private readonly addressRepository: Repository<Address>,
    @InjectRepository(Store)
    private readonly storeRepository: Repository<Store>,

    private readonly feeShippingService: FeeShippingService,
    private readonly configService: ConfigService,
  ) {
    this.api_key_dev = this.configService.get('API_KEY_DEV');
    this.shop_id_dev = this.configService.get('SHOP_ID_DEV');
    this.url_api_create_order = this.configService.get(
      'URL_API_CREATE_ORDER_DEV',
    );
    this.url_api_cancel_order = this.configService.get('ULR_API_CANCEL_ORDER');
    this.config = {
      headers: {
        'Content-Type': 'application/json',
        token: this.api_key_dev,
        ShopId: this.shop_id_dev,
      },
    };
  }

  async checkUserId(userId: string): Promise<void> {
    const user = await this.userRepository.findOne({ where: { id: userId } });
    if (!user) {
      throw new NotFoundException({
        statusCode: HttpStatus.NOT_FOUND,
        message: 'error user not found',
      });
    }
  }

  async checkAddressId(addressId: string): Promise<Address> {
    const address = await this.addressRepository.findOne({
      where: { id: addressId },
    });
    if (!address) {
      throw new NotFoundException({
        statusCode: HttpStatus.NOT_FOUND,
        message: 'ERROR : address not found',
      });
    }
    return address;
  }

  async checkStoreId(storeId: string): Promise<Store> {
    const store = await this.storeRepository.findOne({
      where: { id: storeId },
    });
    if (!store) {
      throw new NotFoundException({
        statusCode: HttpStatus.NOT_FOUND,
        message: 'ERROR : store not found',
      });
    }
    return store;
  }

  async checkCartId(cartId: string): Promise<Cart> {
    const cart = await this.cartRepository.findOne({
      where: { id: cartId },
      relations: ['cartItems', 'cartItems.product'],
    });
    if (!cart) {
      throw new NotFoundException({
        statusCode: HttpStatus.NOT_FOUND,
        message: 'ERROR : cart not found',
      });
    }
    if (cart.orderId) {
      throw new BadRequestException({
        statusCode: HttpStatus.BAD_REQUEST,
        message: 'ERROR : Cart already has an orderId',
      });
    }
    return cart;
  }

  async checkOrderId(orderId: string): Promise<Order> {
    const order = await this.orderRepository.findOne({
      where: { id: orderId },
      relations: ['carts', 'carts.cartItems', 'carts.cartItems.product'],
    });
    if (!order) {
      throw new NotFoundException({
        statusCode: HttpStatus.NOT_FOUND,
        message: 'order not found',
      });
    }
    return order;
  }

  private async transformCartItems(cart: Cart): Promise<
    {
      name: string;
      quantity: number;
      weight: number;
    }[]
  > {
    return cart.cartItems.map((item) => {
      return {
        name: item.product.title,
        quantity: item.quantity,
        weight: item.product.weight * item.quantity,
      };
    });
  }

  async calculateTotalMoney(cart: Cart): Promise<number> {
    return cart.cartItems.reduce((cartTotal, cartItem) => {
      const product = cartItem.product;
      const quantity = cartItem.quantity;
      return cartTotal + product.price * quantity;
    }, 0);
  }

  async calculateFeeShipping(
    address: Address,
    cart: Cart,
    totalWeight: number,
    items: any,
  ): Promise<number> {
    const store = await this.checkStoreId(cart.storeId);
    const fee = await this.feeShippingService.calculateFeeShipping({
      from_province_name: store.province,
      from_district_name: store.district,
      from_ward_name: store.ward,
      to_province_name: address.province,
      to_district_name: address.district,
      to_ward_name: address.ward,
      weight: totalWeight,
      items: items,
    });
    return fee;
  }

  async calculateTotalWeight(cart: Cart): Promise<number> {
    return cart.cartItems.reduce((total, item) => {
      return total + item.product.weight * item.quantity;
    }, 0);
  }

  async createOrderGHN(
    address: Address,
    cart: Cart,
    totalMoney: number,
    feeShipping: number,
    totalWeight: number,
    note: string,
    items: any,
  ): Promise<GHNResponseData> {
    const store = await this.checkStoreId(cart.storeId);
    const dataOrderGHN = {
      service_type_id: 2,
      payment_type_id: 1,
      required_note: 'KHONGCHOXEMHANG',
      from_name: store.name,
      from_phone: store.phoneNumber,
      from_address: store.addressDetail,
      from_ward_name: await removeDiacriticsAndSpaces(store.ward),
      from_district_name: await removeDiacriticsAndSpaces(store.district),
      from_province_name: await removeDiacriticsAndSpaces(store.province),
      to_name: address.name,
      to_phone: address.phoneNumber,
      to_address: address.detailAddress,
      to_ward_name: await removeDiacriticsAndSpaces(address.ward),
      to_district_name: await removeDiacriticsAndSpaces(address.district),
      to_province_name: await removeDiacriticsAndSpaces(address.province),
      cod_amount: feeShipping + totalMoney,
      note: note,
      weight: totalWeight,
      insurance_value: totalMoney,
      items: items,
    };

    try {
      const response = await axios.post(
        this.url_api_create_order,
        dataOrderGHN,
        this.config,
      );
      return response.data.data;
    } catch (error) {
      throw new BadRequestException({
        statusCode: HttpStatus.BAD_REQUEST,
        message:
          'GHN API Error:' + error.response
            ? error.response.data
            : error.message,
      });
    }
  }

  async create(orderData: CreateOrderDto): Promise<Order> {
    await this.checkUserId(orderData.userId);
    const address = await this.checkAddressId(orderData.addressId);
    const cart = await this.checkCartId(orderData.cartId);
    const items = await this.transformCartItems(cart);

    const totalMoney: number = await this.calculateTotalMoney(cart);
    const totalWeight: number = await this.calculateTotalWeight(cart);
    const feeShipping: number = await this.calculateFeeShipping(
      address,
      cart,
      totalWeight,
      items,
    );

    const newOrder = await this.orderRepository.create({
      userId: orderData.userId,
      addressId: orderData.addressId,
      note: orderData.note,
      orderDate: new Date(),
      totalMoney: totalMoney + feeShipping,
    });

    const orderGHN = await this.createOrderGHN(
      address,
      cart,
      totalMoney,
      feeShipping,
      totalWeight,
      orderData.note,
      items,
    );
    newOrder.expectedDeliveryTime = orderGHN.expected_delivery_time;
    newOrder.orderCode = orderGHN.order_code;
    await this.orderRepository.save(newOrder);
    //await this.updateCart(cart, newOrder.id);
    return newOrder;
  }

  async updateCart(cart: Cart, orderId: string): Promise<void> {
    try {
      cart.orderId = orderId;
      await this.cartRepository.save(cart);
    } catch (error) {
      throw new BadRequestException({
        statusCode: HttpStatus.BAD_REQUEST,
        message: 'error update cart ' + error.message,
      });
    }
  }

  async getDetail(id: string): Promise<Order> {
    return await this.checkOrderId(id);
  }

  async getList(filter: OrderFilterType): Promise<OrderPaginationResponseType> {
    const itemsPerPage: number = Number(filter.items_per_page) || 10;
    const page: number = filter.page || 1;
    const search: string = filter.search || '';
    const skip = page > 1 ? (page - 1) * itemsPerPage : 0;
    let sort_by: string = filter.sort_by || 'createdAt';
    const order_by = filter.order_by?.toLowerCase() === 'desc' ? 'DESC' : 'ASC';
    const userId: string = filter.userId || '';
    const status: number = filter.status || null;

    if (!SORT_BY_ORDER.includes(sort_by)) {
      sort_by = 'createdAt';
    }

    const query = this.orderRepository.createQueryBuilder('order');

    if (userId || status !== null) {
      query.where(
        new Brackets((qb) => {
          if (userId) {
            qb.andWhere('order.userId = :userId', { userId });
          }
          if (status !== null) {
            qb.andWhere('order.status = :status', { status });
          }
        }),
      );
    }

    if (search) {
      query.andWhere(
        new Brackets((qb) => {
          qb.where('order.title LIKE :search', { search: `%${search}%` })
            .orWhere('order.fullname LIKE :search', {
              search: `%${search}%`,
            })
            .orWhere('order.email LIKE :search', {
              search: `%${search}%`,
            })
            .orWhere('order.phoneNumber LIKE :search', {
              search: `%${search}%`,
            })
            .orWhere('order.address LIKE :search', {
              search: `%${search}%`,
            })
            .orWhere('order.note LIKE :search', {
              search: `%${search}%`,
            });
        }),
      );
    }

    query.leftJoinAndSelect('order.carts', 'carts');
    query.leftJoinAndSelect('carts.cartItems', 'cartItems');
    query.leftJoinAndSelect('cartItems.product', 'product');

    query.orderBy(`order.${sort_by}`, order_by);
    query.skip(skip);
    query.take(itemsPerPage);

    const [data, total] = await query.getManyAndCount();

    return {
      data,
      total,
      currentPage: page,
      itemsPerPage,
    };
  }

  async cancel(id: string): Promise<Order> {
    const order = await this.checkOrderId(id);
    if (!OrderStatusCancel.includes(order.status)) {
      throw new BadRequestException({
        statusCode: HttpStatus.BAD_REQUEST,
        message: 'ERROR : Orders cannot be canceled ',
      });
    }

    return;
  }
}
