import { Body, Controller, Post, Get, Param, Query } from '@nestjs/common';
import { OrderService } from './order.service';
import { CreateOrderDto } from './dtos/order.create.dto';
import { Roles } from 'src/auth/decorators/role.decorator';
import { Order } from 'src/common/database/entities/Order';
import { OrderFilterType } from './interfaces/order.filter-type.interface';
import { OrderPaginationResponseType } from './interfaces/order.pagination-response-type.interface';
import { ApiTags } from '@nestjs/swagger';
import {
  OrderCreateDoc,
  OrderGetListDoc,
  OrderGetDetailDoc,
} from './docs/order.doc';

@ApiTags('Order')
@Controller('order')
export class OrderController {
  constructor(private readonly orderService: OrderService) {}

  @OrderCreateDoc()
  @Post()
  @Roles('admin', 'user', 'store')
  async get(@Body() data: CreateOrderDto): Promise<Order> {
    return await this.orderService.create(data);
  }

  @OrderGetDetailDoc()
  @Get(':id')
  @Roles('admin', 'user', 'store')
  getDetail(@Param('id') id: string): Promise<Order> {
    return this.orderService.getDetail(id);
  }

  @OrderGetListDoc()
  @Get()
  @Roles('admin', 'store', 'admin')
  getAll(
    @Query() filter: OrderFilterType,
  ): Promise<OrderPaginationResponseType> {
    return this.orderService.getList(filter);
  }
}
