import { faker } from '@faker-js/faker';
import { ApiProperty } from '@nestjs/swagger';
import {
  IsArray,
  IsEmail,
  IsEnum,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
  Min,
} from 'class-validator';
import { PAYMENT_METHOD } from '../constants/order.payment-method.constants';

export class CreateOrderDto {
  @ApiProperty({
    example: faker.string.uuid(),
    required: true,
  })
  @IsNotEmpty()
  @IsString()
  userId: string;

  @ApiProperty({
    example: faker.string.uuid(),
    required: true,
  })
  @IsNotEmpty()
  @IsString()
  addressId: string;

  @ApiProperty({
    example: faker.lorem.words(5),
    required: true,
  })
  @IsNotEmpty()
  @IsOptional()
  note: string;

  @ApiProperty({
    example: faker.helpers.enumValue(PAYMENT_METHOD),
    required: true,
  })
  @IsEnum(PAYMENT_METHOD)
  @IsOptional()
  paymentMethod: PAYMENT_METHOD;

  @ApiProperty({
    example: faker.string.uuid(),
    required: true,
  })
  @IsOptional()
  @IsString()
  paymentId: string;

  @ApiProperty({
    example: faker.string.uuid(),
    required: true,
  })
  @IsNotEmpty()
  @IsString()
  cartId: string;
}
