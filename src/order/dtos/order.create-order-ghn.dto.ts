import { ItemDto } from 'src/fee-shipping/dtos/fee-shipping.calculate.dto';

export class OrderCreateGHNDto {
  payment_type_id: number;
  from_name: string;
  from_phone: string;
  from_address: string;
  from_ward_name: string;
  from_district_name: string;
  from_province_name: string;
  to_name: string;
  to_phone: string;
  to_address: string;
  to_ward_code: string;
  to_district_id: number;
  cod_amount: number;
  content: string;
  weight: number;
  insurance_value: number;
  items: ItemDto[];
}

