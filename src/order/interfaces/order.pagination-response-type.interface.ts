import { Order } from 'src/common/database/entities/Order';

export interface OrderPaginationResponseType {
  data: Order[];
  total: number;
  currentPage: number;
  itemsPerPage: number;
}
