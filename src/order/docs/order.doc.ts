import { applyDecorators } from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiParam,
  ApiQuery,
  ApiResponse,
} from '@nestjs/swagger';
import {
  OrderCreateSerialization,
  OrderGetDetailSerialization,
} from '../serializations/order.serialization';
import { faker } from '@faker-js/faker';

export function OrderCreateDoc(): MethodDecorator {
  return applyDecorators(
    ApiBearerAuth(),
    ApiOperation({ summary: 'module order' }),
    ApiResponse({
      status: 200,
      description: 'Success',
      type: OrderCreateSerialization,
    }),
  );
}

export function OrderGetListDoc(): MethodDecorator {
  return applyDecorators(
    ApiBearerAuth(),
    ApiOperation({ summary: 'module order' }),
    ApiQuery({
      name: 'items_per_page',
      example: faker.number.int(20),
      type: 'number',
    }),
    ApiBearerAuth(),
    ApiQuery({
      name: 'page',
      example: faker.number.int(20),
      type: 'number',
    }),
    ApiQuery({
      name: 'search',
      example: 'meat',
      type: 'string',
    }),
    ApiQuery({
      name: 'sort_by',
      example: 'email',
      type: 'string',
    }),
    ApiQuery({
      name: 'order_by',
      example: 'desc',
      type: 'string',
    }),
    ApiQuery({
      name: 'userId',
      example: faker.string.uuid(),
      type: 'string',
    }),
    ApiResponse({
      status: 200,
      description: 'Success',
      type: OrderCreateSerialization,
    }),
  );
}

export function OrderGetDetailDoc(): MethodDecorator {
  return applyDecorators(
    ApiBearerAuth(),
    ApiOperation({ summary: 'module order' }),
    ApiParam({
      name: 'id',
      example: faker.string.uuid(),
      type: 'string',
    }),
    ApiResponse({
      status: 200,
      description: 'Success',
      type: OrderGetDetailSerialization,
    }),
  );
}
