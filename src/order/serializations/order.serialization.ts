import { faker } from '@faker-js/faker';
import { ApiProperty } from '@nestjs/swagger';
import { OrderPaginationResponseType } from '../interfaces/order.pagination-response-type.interface';
import { Order } from 'src/common/database/entities/Order';

export class OrderCreateSerialization {
  @ApiProperty({
    example: faker.string.uuid(),
    required: true,
    nullable: false,
  })
  id: string;

  @ApiProperty({
    example: faker.string.uuid(),
    required: true,
    nullable: false,
  })
  readonly userId: string;

  @ApiProperty({
    example: faker.person.fullName(),
    required: true,
    nullable: false,
  })
  readonly fullname: string;

  @ApiProperty({
    example: faker.internet.email(),
    required: true,
    nullable: false,
  })
  readonly email: string;

  @ApiProperty({
    example: faker.phone.number(),
    required: true,
    nullable: false,
  })
  readonly phoneNumber: string;

  @ApiProperty({
    example: faker.location.streetAddress(),
    required: true,
    nullable: false,
  })
  readonly address: string;

  @ApiProperty({
    example: faker.word.words(5),
    required: true,
    nullable: false,
  })
  readonly note: string;

  @ApiProperty({
    example: faker.date.birthdate(),
    required: true,
    nullable: false,
  })
  readonly orderDate: Date;

  @ApiProperty({
    example: faker.number.int(3),
    required: true,
    nullable: false,
  })
  readonly status: number;

  @ApiProperty({
    example: faker.string.uuid(),
    required: true,
    nullable: false,
  })
  readonly paymentId: string;

  @ApiProperty({
    example: faker.commerce.price(),
    required: true,
    nullable: false,
  })
  readonly totalMoney: number;

  @ApiProperty({
    example: faker.string.uuid(),
    required: true,
    nullable: false,
  })
  readonly shippingId: string;

  @ApiProperty({
    example: faker.date.birthdate(),
    required: true,
    nullable: false,
  })
  readonly createdAt: Date;

  @ApiProperty({
    example: faker.date.birthdate(),
    required: true,
    nullable: false,
  })
  readonly updatedAt: Date;
}

export class ProductGetListSerialization
  implements OrderPaginationResponseType
{
  @ApiProperty({
    type: [OrderCreateSerialization],
    required: true,
    nullable: false,
  })
  data: Order[];

  @ApiProperty({
    example: faker.number.int(10),
    required: true,
    nullable: false,
  })
  total: number;

  @ApiProperty({
    example: faker.number.int(10),
    required: true,
    nullable: false,
  })
  currentPage: number;

  @ApiProperty({
    example: faker.number.int(10),
    required: true,
    nullable: false,
  })
  itemsPerPage: number;
}

export class OrderGetDetailSerialization extends OrderCreateSerialization {}
