import { faker } from '@faker-js/faker';
import { ApiProperty } from '@nestjs/swagger';
import { FeedbackPaginationResponseType } from '../interfaces/feedback.pagination-response-type.interface';
import { Feedback } from 'src/common/database/entities/Feedback';
import { DeleteResponse } from 'src/common/helper/interfaces/helper.response-delete';

export class FeedbackCreateSerialization {
  @ApiProperty({
    example: faker.string.uuid(),
    required: true,
    nullable: false,
  })
  readonly id: string;

  @ApiProperty({
    example: faker.string.uuid(),
    required: true,
    nullable: false,
  })
  readonly userId: string;

  @ApiProperty({
    example: faker.string.uuid(),
    required: true,
    nullable: false,
  })
  readonly productId: string;

  @ApiProperty({
    example: faker.number.int(5),
    required: true,
    nullable: false,
  })
  readonly rating: number;

  @ApiProperty({
    example: faker.word.words(5),
    required: true,
    nullable: false,
  })
  readonly comment: string;

  @ApiProperty({
    example: faker.number.int(2),
    required: true,
    nullable: false,
  })
  readonly status: number;

  @ApiProperty({
    example: faker.date.birthdate(),
    required: true,
    nullable: false,
  })
  readonly createdAt: Date;

  @ApiProperty({
    example: faker.date.birthdate(),
    required: true,
    nullable: false,
  })
  readonly updatedAt: Date;
}

export class FeedbackUpdateSerialization extends FeedbackCreateSerialization {}

export class FeedbackGetDetailSerialization extends FeedbackCreateSerialization {}

export class FeedbackGetListSerialization
  implements FeedbackPaginationResponseType
{
  @ApiProperty({
    type: [FeedbackCreateSerialization],
    required: true,
    nullable: false,
  })
  data: Feedback[];
  @ApiProperty({
    example: faker.number.int(10),
    required: true,
    nullable: false,
  })
  total: number;
  @ApiProperty({
    example: faker.number.int(10),
    required: true,
    nullable: false,
  })
  currentPage: number;
  @ApiProperty({
    example: faker.number.int(10),
    required: true,
    nullable: false,
  })
  itemsPerPage: number;
}

export class FeedbackDeleteSerialization implements DeleteResponse<Feedback> {
  @ApiProperty({
    example: 'delete feedback successful',
  })
  message: string;
  @ApiProperty({
    type: [FeedbackUpdateSerialization],
    required: true,
    nullable: false,
  })
  data: Feedback;
}
