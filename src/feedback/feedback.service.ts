import { HttpStatus, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Feedback } from 'src/common/database/entities/Feedback';
import { Product } from 'src/common/database/entities/Product';
import { User } from 'src/common/database/entities/User';
import { Brackets, Like, Repository } from 'typeorm';
import { CreateFeedbackDto } from './dtos/feedback.create.dto';
import { UpdateFeedbackDto } from './dtos/feedback.update.dto';
import { FeedbackFilterType } from './interfaces/feedback.filter-type.interface';
import { FeedbackPaginationResponseType } from './interfaces/feedback.pagination-response-type.interface';
import { SORT_BY_FEEDBACK } from './constants/feedback.sort-by.constants';
import { DeleteResponse } from 'src/common/helper/interfaces/helper.response-delete';

@Injectable()
export class FeedbackService {
  constructor(
    @InjectRepository(Feedback)
    private readonly feedbackRepository: Repository<Feedback>,
    @InjectRepository(Product)
    private readonly productRepository: Repository<Product>,
    @InjectRepository(User) private readonly userRepository: Repository<User>,
  ) {}

  async checkBodyId(productId: string, userId: string): Promise<void> {
    const user = await this.userRepository.findOne({
      where: { id: userId },
    });
    if (!user) {
      throw new NotFoundException({
        statusCode: HttpStatus.NOT_FOUND,
        message: 'error user not found',
      });
    }

    const product = await this.productRepository.findOne({
      where: { id: productId },
    });
    if (!product) {
      throw new NotFoundException({
        statusCode: HttpStatus.NOT_FOUND,
        message: 'error product not found',
      });
    }
  }

  async create(feedbackData: CreateFeedbackDto): Promise<Feedback> {
    await this.checkBodyId(feedbackData.productId, feedbackData.userId);

    const feedbackNew = await this.feedbackRepository.create({
      productId: feedbackData.productId,
      userId: feedbackData.userId,
      rating: feedbackData.rating,
      comment: feedbackData.comment,
    });

    return await this.feedbackRepository.save(feedbackNew);
  }

  async update(id: string, feedbackData: UpdateFeedbackDto): Promise<Feedback> {
    const feedback = await this.feedbackRepository.findOne({
      where: { id: id },
    });
    if (!feedback) {
      throw new NotFoundException({
        statusCode: HttpStatus.NOT_FOUND,
        message: 'error feedback not found',
      });
    }
    await this.checkBodyId(feedbackData.productId, feedbackData.userId);

    feedback.productId = feedbackData.productId;
    feedback.userId = feedbackData.userId;
    feedback.rating = feedbackData.rating;
    feedback.comment = feedbackData.comment;
    feedback.status = 1;
    feedback.updatedAt = new Date();
    return await this.feedbackRepository.save(feedback);
  }

  async getAll(
    filter: FeedbackFilterType,
  ): Promise<FeedbackPaginationResponseType> {
    const itemsPerPage: number = Number(filter.items_per_page) || 10;
    const page: number = filter.page || 1;
    const search: string = filter.search || '';
    const skip = page - 1 ? (page - 1) * itemsPerPage : 0;
    let sort_by: string = filter.sort_by || 'createdAt';
    const order_by = filter.order_by?.toLowerCase() === 'desc' ? 'DESC' : 'ASC';

    const productId: string = filter.productId || '';
    const userId: string = filter.userId || '';

    const query = this.feedbackRepository.createQueryBuilder('feedback');

    if (productId || userId) {
      query.andWhere(
        new Brackets((qb) => {
          if (productId) {
            qb.andWhere('feedback.productId = :productId', { productId });
          }
          if (userId) {
            qb.andWhere('feedback.userId = :userId', { userId });
          }
        }),
      );
    }

    if (search) {
      query.andWhere(
        new Brackets((qb) => {
          qb.where('feedback.comment LIKE :search', {
            search: `%${search}%`,
          }).orWhere('feedback.rating LIKE :search', {
            search: `%${search}%`,
          });
        }),
      );
    }

    if (!SORT_BY_FEEDBACK.includes(sort_by)) {
      sort_by = 'createdAt';
    }

    query.orderBy(`feedback.${sort_by}`, order_by);
    query.skip(skip);
    query.take(itemsPerPage);

    const [data, total] = await query.getManyAndCount();

    return {
      data,
      total,
      currentPage: page,
      itemsPerPage,
    };
  }

  async getDetail(id: string): Promise<Feedback> {
    const feedback = await this.feedbackRepository.findOne({
      where: { id: id },
    });
    if (!feedback) {
      throw new NotFoundException({
        statusCode: HttpStatus.NOT_FOUND,
        message: 'error feedback not found',
      });
    }

    return feedback;
  }

  async delete(id: string): Promise<DeleteResponse<Feedback>> {
    const feedback = await this.feedbackRepository.findOne({
      where: { id: id },
    });
    if (!feedback) {
      throw new NotFoundException({
        statusCode: HttpStatus.NOT_FOUND,
        message: 'error feedback not found',
      });
    }

    await this.feedbackRepository.delete({
      id: id,
    });
    return {
      message: 'delete product successful',
      data: feedback,
    };
  }
}
