import {
  Body,
  Controller,
  Param,
  Post,
  Put,
  Get,
  Query,
  Delete,
} from '@nestjs/common';
import { FeedbackService } from './feedback.service';
import { ApiTags } from '@nestjs/swagger';
import { Roles } from '../auth/decorators/role.decorator';
import { CreateFeedbackDto } from './dtos/feedback.create.dto';
import { Feedback } from 'src/common/database/entities/Feedback';
import { UpdateFeedbackDto } from './dtos/feedback.update.dto';
import { FeedbackFilterType } from './interfaces/feedback.filter-type.interface';
import { FeedbackPaginationResponseType } from './interfaces/feedback.pagination-response-type.interface';
import { DeleteResponse } from 'src/common/helper/interfaces/helper.response-delete';
import {
  FeedbackCreateDoc,
  FeedbackDeleteDoc,
  FeedbackGetDetailDoc,
  FeedbackGetListDoc,
  FeedbackUpdateDoc,
} from './docs/feedback.doc';

@ApiTags('Feedback')
@Controller('feedback')
export class FeedbackController {
  constructor(private readonly feedbackService: FeedbackService) {}

  @FeedbackCreateDoc()
  @Post()
  @Roles('admin', 'store', 'user')
  create(@Body() body: CreateFeedbackDto): Promise<Feedback> {
    return this.feedbackService.create(body);
  }

  @FeedbackUpdateDoc()
  @Put(':id')
  @Roles('store', 'admin')
  update(
    @Param('id') id: string,
    @Body() body: UpdateFeedbackDto,
  ): Promise<Feedback> {
    return this.feedbackService.update(id, body);
  }

  @FeedbackGetListDoc()
  @Get()
  @Roles('admin', 'store', 'admin')
  getAll(
    @Query() filter: FeedbackFilterType,
  ): Promise<FeedbackPaginationResponseType> {
    return this.feedbackService.getAll(filter);
  }

  @FeedbackGetDetailDoc()
  @Get(':id')
  @Roles('admin', 'user', 'store')
  getDetail(@Param('id') id: string): Promise<Feedback> {
    return this.feedbackService.getDetail(id);
  }

  @FeedbackDeleteDoc()
  @Delete(':id')
  @Roles('admin', 'store')
  delete(@Param('id') id: string): Promise<DeleteResponse<Feedback>> {
    return this.feedbackService.delete(id);
  }
}
