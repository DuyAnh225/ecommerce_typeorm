import { applyDecorators } from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiParam,
  ApiQuery,
  ApiResponse,
} from '@nestjs/swagger';
import {
  FeedbackCreateSerialization,
  FeedbackDeleteSerialization,
  FeedbackGetDetailSerialization,
  FeedbackGetListSerialization,
  FeedbackUpdateSerialization,
} from '../serializations/feedback.serialization';
import { faker } from '@faker-js/faker';

export function FeedbackCreateDoc(): MethodDecorator {
  return applyDecorators(
    ApiBearerAuth(),
    ApiOperation({ summary: 'module feedback' }),
    ApiResponse({
      status: 201,
      description: 'Success',
      type: FeedbackCreateSerialization,
    }),
  );
}

export function FeedbackUpdateDoc(): MethodDecorator {
  return applyDecorators(
    ApiBearerAuth(),
    ApiOperation({ summary: 'module feedback' }),
    ApiParam({
      name: 'id',
      example: faker.string.uuid(),
      type: 'string',
    }),
    ApiResponse({
      status: 200,
      description: 'Success',
      type: FeedbackUpdateSerialization,
    }),
  );
}

export function FeedbackGetListDoc(): MethodDecorator {
  return applyDecorators(
    ApiBearerAuth(),
    ApiOperation({ summary: 'module feedback' }),
    ApiQuery({
      name: 'items_per_page',
      example: faker.number.int(20),
      type: 'number',
    }),
    ApiQuery({
      name: 'page',
      example: faker.number.int(20),
      type: 'number',
    }),
    ApiQuery({
      name: 'search',
      example: 'meat',
      type: 'string',
    }),
    ApiResponse({
      status: 200,
      description: 'Success',
      type: FeedbackGetListSerialization,
    }),
  );
}

export function FeedbackGetDetailDoc(): MethodDecorator {
  return applyDecorators(
    ApiBearerAuth(),
    ApiOperation({ summary: 'module feedback' }),
    ApiParam({
      name: 'id',
      example: faker.string.uuid(),
      type: 'string',
    }),
    ApiResponse({
      status: 200,
      description: 'Success',
      type: FeedbackGetDetailSerialization,
    }),
  );
}

export function FeedbackDeleteDoc(): MethodDecorator {
  return applyDecorators(
    ApiBearerAuth(),
    ApiOperation({ summary: 'module feedback' }),
    ApiParam({
      name: 'id',
      example: faker.string.uuid(),
      type: 'string',
    }),
    ApiResponse({
      status: 200,
      description: 'Success',
      type: FeedbackDeleteSerialization,
    }),
  );
}
