import { Module } from '@nestjs/common';
import { FeedbackService } from './feedback.service';
import { FeedbackController } from './feedback.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Feedback } from 'src/common/database/entities/Feedback';
import { CartItem } from 'src/common/database/entities/CartItem';
import { Product } from 'src/common/database/entities/Product';
import { Cart } from 'src/common/database/entities/Cart';
import { User } from 'src/common/database/entities/User';

@Module({
  imports: [TypeOrmModule.forFeature([Feedback, Cart, CartItem, Product,User])],
  controllers: [FeedbackController],
  providers: [FeedbackService],
})
export class FeedbackModule {}
