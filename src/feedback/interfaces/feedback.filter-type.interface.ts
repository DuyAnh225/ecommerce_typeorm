export interface FeedbackFilterType {
  items_per_page?: number;
  page?: number;
  search?: string;
  sort_by?: string;
  order_by?: string;
  userId?: string;
  productId?: string;
}
