import { Feedback } from 'src/common/database/entities/Feedback';

export interface FeedbackPaginationResponseType {
  data: Feedback[];
  total: number;
  currentPage: number;
  itemsPerPage: number;
}
