import { CreateFeedbackDto } from './feedback.create.dto';

export class UpdateFeedbackDto extends CreateFeedbackDto {}
