import { Store } from 'src/common/database/entities/Store';

export interface StorePaginationResponseType {
  data: Store[];
  total: number;
  currentPage: number;
  itemsPerPage: number;
}
