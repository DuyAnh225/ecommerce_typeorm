import {
  BadRequestException,
  HttpStatus,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Store } from 'src/common/database/entities/Store';
import { Brackets, Like, Repository } from 'typeorm';
import { CreateStoreDto } from './dtos/store.create.dto';
import { User } from 'src/common/database/entities/User';
import { UpdateStoreDto } from './dtos/store.update.dto';
import { StoreFilterType } from './interfaces/store.filter-type.interface';
import { StorePaginationResponseType } from './interfaces/store.pagination-response-type.interface';
import { SORT_BY_STORE } from './constants/store.sort-by.constant';
import { DeleteResponse } from 'src/common/helper/interfaces/helper.response-delete';
import { MediaService } from 'src/media/media.service';

@Injectable()
export class StoreService {
  constructor(
    @InjectRepository(Store)
    private readonly storeRepository: Repository<Store>,
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
    private readonly mediaService: MediaService,
  ) {}

  async create(storeData: CreateStoreDto): Promise<Store> {
    const store = await this.storeRepository.findOne({
      where: { name: storeData.name },
    });
    if (store) {
      throw new BadRequestException({
        statusCode: HttpStatus.BAD_REQUEST,
        message: 'store is exist',
      });
    }
    const user = await this.userRepository.findOne({
      where: { id: storeData.userId, role: 'user' },
    });

    if (!user) {
      throw new BadRequestException({
        statusCode: HttpStatus.NOT_FOUND,
        message: 'user not found or created a store',
      });
    }
    const storeNew = await this.storeRepository.create({
      userId: storeData.userId,
      name: storeData.name,
      type: storeData.type,
      addressDetail: storeData.addressDetail,
      province: storeData.province,
      district: storeData.district,
      ward: storeData.ward,
      country: storeData.country,
      webAddress: storeData.webAddress,
      courierName: storeData.courierName,
      tagLine: storeData.tagLine,
      description: storeData.description,
    });
    user.role = 'store';
    await this.userRepository.save(user);

    return await this.storeRepository.save(storeNew);
  }

  async update(id: string, storeData: UpdateStoreDto): Promise<Store> {
    const store = await this.storeRepository.findOne({
      where: { id: id },
    });
    if (!store) {
      throw new BadRequestException({
        statusCode: HttpStatus.BAD_REQUEST,
        message: 'store does not exist',
      });
    }
    const checkNameStore = await this.storeRepository.findOne({
      where: { name: storeData.name },
    });

    if (checkNameStore) {
      throw new BadRequestException({
        statusCode: HttpStatus.BAD_REQUEST,
        message: 'store name is exist',
      });
    }

    store.name = storeData.name;
    store.type = storeData.type;
    store.addressDetail = storeData.addressDetail;
    store.province = storeData.province;
    store.district = storeData.district;
    store.ward = storeData.ward;
    store.country = storeData.country;
    store.webAddress = storeData.webAddress;
    store.courierName = storeData.courierName;
    store.tagLine = storeData.tagLine;
    store.updatedAt = new Date();

    return await this.storeRepository.save(store);
  }

  async getAll(filter: StoreFilterType): Promise<StorePaginationResponseType> {
    const itemsPerPage: number = Number(filter.items_per_page) || 10;
    const page: number = filter.page || 1;
    const search: string = filter.search || '';
    const skip = page - 1 ? (page - 1) * itemsPerPage : 0;
    let sort_by: string = filter.sort_by || 'created_at';
    const order_by = filter.order_by?.toLowerCase() === 'desc' ? 'DESC' : 'ASC';

    if (!SORT_BY_STORE.includes(sort_by)) {
      sort_by = 'createdAt';
    }

    const userId: string = filter.userId || '';

    const query = this.storeRepository.createQueryBuilder('store');

    if (userId) {
      query.andWhere(
        new Brackets((qb) => {
          if (userId) {
            qb.andWhere('store.userId = :userId', { userId });
          }
        }),
      );
    }

    if (search) {
      query.andWhere(
        new Brackets((qb) => {
          qb.where('store.name LIKE :search', { search: `%${search}%` })
            .orWhere('store.description LIKE :search', {
              search: `%${search}%`,
            })
            .orWhere('store.city LIKE :search', {
              search: `%${search}%`,
            })
            .orWhere('store.country LIKE :search', {
              search: `%${search}%`,
            })
            .orWhere('store.type LIKE :search', {
              search: `%${search}%`,
            });
        }),
      );
    }

    query.orderBy(`store.${sort_by}`, order_by);
    query.skip(skip);
    query.take(itemsPerPage);

    const [data, total] = await query.getManyAndCount();

    return {
      data,
      total,
      currentPage: page,
      itemsPerPage,
    };
  }

  async getDetail(id: string): Promise<Store> {
    const store = await this.storeRepository.findOne({
      where: { id: id },
    });
    if (!store) {
      throw new NotFoundException({
        statusCode: HttpStatus.NOT_FOUND,
        message: 'store not found',
      });
    }
    return store;
  }

  async delete(id: string): Promise<DeleteResponse<Store>> {
    const store = await this.storeRepository.findOne({
      where: { id: id },
    });
    if (!store) {
      throw new NotFoundException({
        statusCode: HttpStatus.NOT_FOUND,
        message: 'store not found',
      });
    }

    const user = await this.userRepository.findOne({
      where: { id: store.userId },
    });
    user.role = 'user';
    await this.userRepository.save(user);
    await this.storeRepository.delete({
      id: store.id,
    });
    return {
      message: 'delete store successful',
      data: store,
    };
  }

  async uploadAvatar(id: string, file: Express.Multer.File): Promise<Store> {
    const store = await this.storeRepository.findOne({
      where: { id: id },
    });
    if (!store) {
      throw new NotFoundException({
        statusCode: HttpStatus.NOT_FOUND,
        message: 'store not found',
      });
    }

    const location = await this.mediaService.upload(file);

    store.avatar = location;
    return this.storeRepository.save(store);
  }
}
