import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UploadedFile,
} from '@nestjs/common';
import { StoreService } from './store.service';
import { Roles } from 'src/auth/decorators/role.decorator';
import { CreateStoreDto } from './dtos/store.create.dto';
import { Store } from 'src/common/database/entities/Store';
import { UpdateStoreDto } from './dtos/store.update.dto';
import { StoreFilterType } from './interfaces/store.filter-type.interface';
import { StorePaginationResponseType } from './interfaces/store.pagination-response-type.interface';
import { DeleteResponse } from 'src/common/helper/interfaces/helper.response-delete';
import { ApiTags } from '@nestjs/swagger';
import { UseInterceptors } from '@nestjs/common';
import {
  StoreCreateDoc,
  StoreDeleteDoc,
  StoreGetDetailDoc,
  StoreGetListDoc,
  StoreUpdateDoc,
} from './docs/store.doc';
import { FileInterceptor } from '@nestjs/platform-express';
import { StoreUploadAvatar } from './docs/store.doc';

@ApiTags('Store')
@Controller('store')
export class StoreController {
  constructor(private readonly storeService: StoreService) {}

  @StoreCreateDoc()
  @Post()
  @Roles('user', 'admin')
  create(@Body() body: CreateStoreDto): Promise<Store> {
    return this.storeService.create(body);
  }

  @StoreUpdateDoc()
  @Put(':id')
  @Roles('store', 'admin')
  update(
    @Param('id') id: string,
    @Body() body: UpdateStoreDto,
  ): Promise<Store> {
    return this.storeService.update(id, body);
  }

  @StoreGetListDoc()
  @Get()
  @Roles('user', 'store', 'admin')
  getAll(
    @Query() filter: StoreFilterType,
  ): Promise<StorePaginationResponseType> {
    return this.storeService.getAll(filter);
  }

  @StoreGetDetailDoc()
  @Get(':id')
  @Roles('admin', 'user', 'store')
  getDetail(@Param('id') id: string): Promise<Store> {
    return this.storeService.getDetail(id);
  }

  @StoreDeleteDoc()
  @Delete(':id')
  @Roles('admin', 'store')
  delete(@Param('id') id: string): Promise<DeleteResponse<Store>> {
    return this.storeService.delete(id);
  }

  @StoreUploadAvatar()
  @Post('upload/:id')
  @UseInterceptors(FileInterceptor('file'))
  upload(
    @Param('id') id: string,
    @UploadedFile() file: Express.Multer.File,
  ): Promise<Store> {
    return this.storeService.uploadAvatar(id, file);
  }
}
