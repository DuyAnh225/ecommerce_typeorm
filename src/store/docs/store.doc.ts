import { applyDecorators } from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiBody,
  ApiConsumes,
  ApiOperation,
  ApiParam,
  ApiQuery,
  ApiResponse,
} from '@nestjs/swagger';
import {
  StoreCreateSerialization,
  StoreDeleteSerialization,
  StoreGetDetailSerialization,
  StoreGetListSerialization,
  StoreUpdateSerialization,
  StoreUploadAvatarSerialization,
} from '../serializations/store.serialization';
import { faker } from '@faker-js/faker';

export function StoreCreateDoc(): MethodDecorator {
  return applyDecorators(
    ApiBearerAuth(),
    ApiOperation({ summary: 'module store' }),
    ApiResponse({
      status: 200,
      description: 'Success',
      type: StoreCreateSerialization,
    }),
  );
}

export function StoreUpdateDoc(): MethodDecorator {
  return applyDecorators(
    ApiBearerAuth(),
    ApiOperation({ summary: 'module store' }),
    ApiParam({
      name: 'id',
      example: faker.string.uuid(),
      type: 'string',
    }),
    ApiResponse({
      status: 200,
      description: 'Success',
      type: StoreUpdateSerialization,
    }),
  );
}

export function StoreGetListDoc(): MethodDecorator {
  return applyDecorators(
    ApiBearerAuth(),
    ApiOperation({ summary: 'module store' }),
    ApiQuery({
      name: 'items_per_page',
      example: faker.number.int(20),
      type: 'number',
      required: false,
    }),
    ApiQuery({
      name: 'page',
      example: faker.number.int(20),
      type: 'number',
      required: false,
    }),
    ApiQuery({
      name: 'search',
      example: 'meat',
      type: 'string',
      required: false,
    }),
    ApiResponse({
      status: 200,
      description: 'Success',
      type: StoreGetListSerialization,
    }),
  );
}

export function StoreGetDetailDoc(): MethodDecorator {
  return applyDecorators(
    ApiBearerAuth(),
    ApiOperation({ summary: 'module store' }),
    ApiParam({
      name: 'id',
      example: faker.string.uuid(),
      type: 'string',
    }),
    ApiResponse({
      status: 200,
      description: 'Success',
      type: StoreGetDetailSerialization,
    }),
  );
}

export function StoreDeleteDoc(): MethodDecorator {
  return applyDecorators(
    ApiBearerAuth(),
    ApiOperation({ summary: 'module store' }),
    ApiParam({
      name: 'id',
      example: faker.string.uuid(),
      type: 'string',
    }),
    ApiResponse({
      status: 200,
      description: 'Success',
      type: StoreDeleteSerialization,
    }),
  );
}

export function StoreUploadAvatar(): MethodDecorator {
  return applyDecorators(
    ApiBearerAuth(),
    ApiOperation({ summary: 'module store' }),
    ApiParam({
      name: 'id',
      example: faker.string.uuid(),
      type: 'string',
    }),
    ApiConsumes('multipart/form-data'),
    ApiBody({
      schema: {
        type: 'object',
        properties: {
          file: {
            type: 'string',
            format: 'binary',
          },
        },
      },
    }),
    ApiResponse({
      status: 200,
      description: 'Success',
      type: StoreUploadAvatarSerialization,
    }),
  );
}
