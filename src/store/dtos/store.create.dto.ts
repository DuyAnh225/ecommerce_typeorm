import { faker } from '@faker-js/faker';
import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import {
  IsMobilePhone,
  IsNotEmpty,
  IsOptional,
  IsString,
} from 'class-validator';

export class CreateStoreDto {
  @ApiProperty({
    example: faker.string.uuid(),
    required: true,
  })
  @IsNotEmpty()
  @IsString()
  userId: string;
  @ApiProperty({
    example: faker.word.words(1),
    required: true,
  })
  @IsNotEmpty()
  @Type(() => String)
  name: string;
  @ApiProperty({
    example: faker.word.words(5),
    required: false,
  })
  @IsOptional()
  @Type(() => String)
  description?: string;

  @ApiProperty({
    example: faker.phone.number(),
    required: true,
  })
  @IsNotEmpty()
  @IsMobilePhone()
  phoneNumber: string;

  @ApiProperty({
    example: 'food',
    required: true,
  })
  @IsOptional()
  @Type(() => String)
  type?: string;
  @ApiProperty({
    example: faker.location.street(),
    required: true,
  })
  @IsNotEmpty()
  @Type(() => String)
  addressDetail: string;

  @ApiProperty({
    example: faker.location.city(),
    required: true,
  })
  @IsNotEmpty()
  @Type(() => String)
  province: string;

  @ApiProperty({
    example: 'nam từ liêm',
    required: true,
  })
  @IsNotEmpty()
  @Type(() => String)
  district: string;

  @ApiProperty({
    example: 'Phú diễn',
    required: true,
  })
  @IsNotEmpty()
  @Type(() => String)
  ward: string;

  @ApiProperty({
    example: faker.location.county(),
    required: true,
  })
  @IsNotEmpty()
  @Type(() => String)
  country: string;

  @ApiProperty({
    example: faker.internet.url(),
    required: true,
  })
  @IsNotEmpty()
  @Type(() => String)
  webAddress: string;

  @ApiProperty({
    example: 'Blue Dart',
    required: true,
  })
  @IsOptional()
  @Type(() => String)
  courierName?: string;

  @ApiProperty({
    example: faker.word.words(5),
    required: false,
  })
  @IsOptional()
  @Type(() => String)
  tagLine?: string;
}
