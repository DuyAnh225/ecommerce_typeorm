export const SORT_BY_STORE: string[] = [
  'createdAt',
  'updatedAt',
  'name',
  'description',
  'city',
  'country',
  'type',
];
