import { faker } from '@faker-js/faker';
import { ApiProperty } from '@nestjs/swagger';
import { StorePaginationResponseType } from '../interfaces/store.pagination-response-type.interface';
import { DeleteResponse } from 'src/common/helper/interfaces/helper.response-delete';
import { Store } from 'src/common/database/entities/Store';

export class StoreCreateSerialization {
  @ApiProperty({
    example: faker.string.uuid(),
    required: true,
    nullable: false,
  })
  readonly id: string;
  @ApiProperty({
    example: faker.string.uuid(),
    required: true,
    nullable: false,
  })
  readonly userId: string;
  @ApiProperty({
    example: faker.word.words(1),
    required: true,
    nullable: false,
  })
  readonly name: string;

  @ApiProperty({
    example: faker.internet.url(),
    required: true,
    nullable: false,
  })
  readonly webAddress: string;

  @ApiProperty({
    example: 'Blue Dart',
    required: true,
    nullable: false,
  })
  readonly courierName: string;

  @ApiProperty({
    example: faker.word.words(5),
    required: true,
    nullable: false,
  })
  readonly tagLine: string;

  @ApiProperty({
    example: faker.word.words(5),
    required: true,
    nullable: false,
  })
  readonly description: string;
  @ApiProperty({
    example: 'food',
    required: true,
    nullable: false,
  })
  readonly type: string;
  @ApiProperty({
    example: faker.location.street(),
    required: true,
    nullable: false,
  })
  readonly address: string;
  @ApiProperty({
    example: faker.location.city(),
    required: true,
    nullable: false,
  })
  readonly city: string;
  @ApiProperty({
    example: faker.location.country(),
    required: true,
    nullable: false,
  })
  readonly country: string;
  @ApiProperty({
    example: faker.date.birthdate(),
    required: true,
    nullable: false,
  })
  readonly createdAt: Date;

  @ApiProperty({
    example: faker.image.url(),
    required: true,
    nullable: false,
  })
  readonly avatar: string;

  @ApiProperty({
    example: null,
    required: true,
    nullable: false,
  })
  readonly updatedAt: Date;
}

export class StoreUpdateSerialization {
  @ApiProperty({
    example: faker.string.uuid(),
    required: true,
    nullable: false,
  })
  readonly id: string;
  @ApiProperty({
    example: faker.string.uuid(),
    required: true,
    nullable: false,
  })
  readonly userId: number;
  @ApiProperty({
    example: faker.word.words(1),
    required: true,
    nullable: false,
  })
  readonly name: string;
  @ApiProperty({
    example: faker.word.words(5),
    required: true,
    nullable: false,
  })
  readonly description: string;
  @ApiProperty({
    example: 'food',
    required: true,
    nullable: false,
  })
  readonly type: string;
  @ApiProperty({
    example: faker.location.street(),
    required: true,
    nullable: false,
  })
  readonly address: string;
  @ApiProperty({
    example: faker.location.city(),
    required: true,
    nullable: false,
  })
  readonly city: string;
  @ApiProperty({
    example: faker.location.country(),
    required: true,
    nullable: false,
  })
  readonly country: string;

  @ApiProperty({
    example: faker.image.url(),
    required: true,
    nullable: false,
  })
  readonly avatar: string;

  @ApiProperty({
    example: faker.internet.url(),
    required: true,
    nullable: false,
  })
  readonly webAddress: string;

  @ApiProperty({
    example: 'Blue Dart',
    required: true,
    nullable: false,
  })
  readonly courierName: string;

  @ApiProperty({
    example: faker.word.words(5),
    required: true,
    nullable: false,
  })
  readonly tagLine: string;

  @ApiProperty({
    example: faker.date.birthdate(),
    required: true,
    nullable: false,
  })
  readonly createdAt: Date;
  @ApiProperty({
    example: faker.date.birthdate(),
    required: true,
    nullable: false,
  })
  readonly updatedAt: Date;
}

export class StoreGetListSerialization implements StorePaginationResponseType {
  @ApiProperty({
    type: [StoreUpdateSerialization],
    required: true,
    nullable: false,
  })
  data: Store[];
  @ApiProperty({
    example: faker.number.int(10),
    required: true,
    nullable: false,
  })
  total: number;
  @ApiProperty({
    example: faker.number.int(10),
    required: true,
    nullable: false,
  })
  currentPage: number;
  @ApiProperty({
    example: faker.number.int(10),
    required: true,
    nullable: false,
  })
  itemsPerPage: number;
}

export class StoreGetDetailSerialization extends StoreUpdateSerialization {}

export class StoreDeleteSerialization implements DeleteResponse<Store> {
  @ApiProperty({
    example: 'delete store successful',
  })
  message: string;
  @ApiProperty({
    type: [StoreUpdateSerialization],
    required: true,
    nullable: false,
  })
  data: Store;
}

export class StoreUploadAvatarSerialization extends StoreUpdateSerialization {}
