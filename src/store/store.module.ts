import { Module } from '@nestjs/common';
import { StoreService } from './store.service';
import { StoreController } from './store.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Store } from 'src/common/database/entities/Store';
import { User } from 'src/common/database/entities/User';
import { MediaService } from '../media/media.service';
import { ConfigService } from '@nestjs/config';

@Module({
  imports: [TypeOrmModule.forFeature([Store, User])],
  controllers: [StoreController],
  providers: [StoreService, ConfigService, MediaService],
})
export class StoreModule {}
