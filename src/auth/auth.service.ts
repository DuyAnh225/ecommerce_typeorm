import {
  BadRequestException,
  HttpStatus,
  Injectable,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { Repository } from 'typeorm';
import { RegisterDto } from './dtos/auth.register.dto';
import { JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';
import { hash, compare } from 'bcryptjs';
import { LoginDto } from './dtos/auth.login.dto';
import { AuthResponseLogin } from './interfaces/auth.response-login';
import { User } from 'src/common/database/entities/User';
import { Token } from 'src/common/database/entities/Token';
import { isValidEmail } from 'src/common/helper/validateEmail';
import { isValidPhoneNumber } from 'src/common/helper/validatePhoneNumber';
import { TaskService } from 'src/task/task.service';

@Injectable()
export class AuthService {
  constructor(
    @InjectRepository(User) private readonly userRepository: Repository<User>,
    @InjectRepository(Token)
    private readonly tokenRepository: Repository<Token>,
    private readonly jwtService: JwtService,
    private readonly configService: ConfigService,
    private readonly taskService: TaskService,
  ) {}

  async register(userRegister: RegisterDto): Promise<User> {
    if (
      !(
        (await isValidEmail(userRegister.emailOrPhone)) ||
        (await isValidPhoneNumber(userRegister.emailOrPhone))
      )
    ) {
      throw new BadRequestException({
        statusCode: HttpStatus.BAD_REQUEST,
        message: 'phone or email is not valid',
      });
    }
    if (userRegister.password !== userRegister.rePassword) {
      throw new BadRequestException({
        statusCode: HttpStatus.BAD_REQUEST,
        message: 'confirm password is not correct',
      });
    }

    const user = await this.userRepository.findOne({
      where: { phoneNumberOrEmail: userRegister.emailOrPhone },
    });
    if (user) {
      throw new BadRequestException({
        statusCode: HttpStatus.BAD_REQUEST,
        message: 'this email(phone number) has been used',
      });
    }

    const hashPassword = await hash(userRegister.password, 10);

    const newUser = await this.userRepository.create({
      firstname: userRegister.firstName,
      lastname: userRegister.lastName,
      phoneNumberOrEmail: userRegister.emailOrPhone,
      password: hashPassword,
      role: 'user',
    });

    return this.userRepository.save(newUser);
  }

  async login(userData: LoginDto): Promise<AuthResponseLogin> {
    const user = await this.userRepository.findOne({
      where: { phoneNumberOrEmail: userData.emailOrPhone },
    });
    if (!user) {
      throw new NotFoundException({
        statusCode: HttpStatus.NOT_FOUND,
        message: 'user does not exists',
      });
    }
    const verifyPassword = await compare(userData.password, user.password);
    if (!verifyPassword) {
      throw new BadRequestException({
        statusCode: HttpStatus.BAD_REQUEST,
        message: 'password is not correct',
      });
    }
    // generate accessToken and refreshToken
    const payload = {
      id: user.id,
      emailOrPhoneNumber: user.phoneNumberOrEmail,
      role: user.role,
    };
    await this.taskService.triggerOrderStatusUpdate(user.id);
    return await this.generateToken(payload);
  }
  private async generateToken(payload: {
    id: string;
    emailOrPhoneNumber: string;
    role: string;
  }) {
    try {
      const accessToken = await this.jwtService.signAsync(payload, {
        secret: this.configService.get<string>('ACCESS_TOKEN_KEY'),
        expiresIn: this.configService.get<string>('EXPIRESIN_ACCESS_TOKEN'),
      });
      const refreshToken = await this.jwtService.signAsync(payload, {
        secret: this.configService.get<string>('REFRESH_TOKEN_KEY'),
        expiresIn: this.configService.get<string>('EXPIRESIN_REFRESH_TOKEN'),
      });
      const newToken = await this.tokenRepository.create({
        userId: payload.id,
        token: refreshToken,
      });
      await this.tokenRepository.save(newToken);
      return { id: payload.id, accessToken, refreshToken };
    } catch (error) {
      throw new BadRequestException({
        statusCode: HttpStatus.BAD_REQUEST,
        message: 'error generate Token ' + error,
      });
    }
  }

  async refreshToken(userId: string): Promise<any> {
    const user = await this.userRepository.findOne({ where: { id: userId } });
    if (!user) {
      throw new NotFoundException({
        statusCode: HttpStatus.NOT_FOUND,
        message: 'error not found',
      });
    }

    const token = await this.tokenRepository.findOne({
      where: { userId: userId },
      order: { createdAt: 'DESC' },
    });
    if (!token) {
      throw new UnauthorizedException({
        message: 'ERROR:The account needs to be logged in at least once',
      });
    }

    try {
      const verifyToken = await this.jwtService.verifyAsync(token.token, {
        secret: this.configService.get<string>('REFRESH_TOKEN_KEY'),
      });
      const payload = {
        id: user.id,
        emailOrPhoneNumber: user.phoneNumberOrEmail,
        role: user.role,
      };
      return await this.generateToken(payload);
    } catch (error) {
      throw new BadRequestException({
        statusCode: HttpStatus.BAD_REQUEST,
        message: 'Refresh token is not valid',
      });
    }
  }
}
