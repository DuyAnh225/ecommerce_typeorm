import { Body, Controller, Post } from '@nestjs/common';
import { AuthService } from './auth.service';
import { RegisterDto } from './dtos/auth.register.dto';
import { Repository } from 'typeorm';

import { LoginDto } from './dtos/auth.login.dto';
import { AuthResponseLogin } from './interfaces/auth.response-login';

import { Public } from './decorators/public.decorator';
import { User } from 'src/common/database/entities/User';
import { ApiTags } from '@nestjs/swagger';
import {
  AuthLoginDoc,
  AuthRefreshToken,
  AuthRegisterDoc,
} from './docs/auth.doc';

@ApiTags('Auth')
@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @AuthRegisterDoc()
  @Public()
  @Post('register')
  register(@Body() body: RegisterDto): Promise<User> {
    return this.authService.register(body);
  }

  @AuthLoginDoc()
  @Public()
  @Post('login')
  login(@Body() body: LoginDto): Promise<AuthResponseLogin> {
    return this.authService.login(body);
  }

  @AuthRefreshToken()
  @Post('refresh-token')
  @Public()
  refreshToken(@Body() { userId }): Promise<AuthResponseLogin> {
    return this.authService.refreshToken(userId);
  }
}
