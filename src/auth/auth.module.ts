import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { TypeOrmModule } from '@nestjs/typeorm';

import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { User } from 'src/common/database/entities/User';
import { Token } from 'src/common/database/entities/Token';
import { TaskService } from 'src/task/task.service';
import { OrderService } from 'src/order/order.service';
import { Order } from 'src/common/database/entities/Order';
import { Cart } from 'src/common/database/entities/Cart';
import { Shipping } from 'src/common/database/entities/Shipping';
import { TaskModule } from 'src/task/task.module';
import { Address } from 'src/common/database/entities/Address';
import { Product } from 'src/common/database/entities/Product';
import { Store } from 'src/common/database/entities/Store';
import { FeeShippingService } from 'src/fee-shipping/fee-shipping.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      User,
      Token,
      Order,
      Cart,
      Shipping,
      Address,
      Store,
      Product
    ]),
    TaskModule,
  ],
  controllers: [AuthController],
  providers: [
    AuthService,
    ConfigService,
    JwtService,
    TaskService,
    OrderService,
    FeeShippingService 
  ],
})
export class AuthModule {}
