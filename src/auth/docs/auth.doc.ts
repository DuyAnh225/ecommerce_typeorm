import { applyDecorators } from '@nestjs/common';
import { ApiBody, ApiOperation, ApiResponse } from '@nestjs/swagger';
import {
  AuthLoginSerialization,
  AuthRefreshTokenSerialization,
  AuthRegisterSerialization,
} from '../serializations/auth.serialization';

export function AuthLoginDoc(): MethodDecorator {
  return applyDecorators(
    ApiOperation({ summary: 'module auth' }),
    ApiResponse({
      status: 201,
      description: 'Success',
      type: AuthLoginSerialization,
    }),
  );
}

export function AuthRegisterDoc(): MethodDecorator {
  return applyDecorators(
    ApiOperation({ summary: 'module auth' }),
    ApiResponse({
      status: 201,
      description: 'Success',
      type: AuthRegisterSerialization,
    }),
  );
}

export function AuthRefreshToken(): MethodDecorator {
  return applyDecorators(
    ApiOperation({ summary: 'module auth' }),
    ApiBody({
      schema: {
        type: 'object',
        properties: {
          userId: { type: 'string' },
        },
        required: ['userId'],
      },
    }),
    ApiResponse({
      status: 201,
      description: 'Success',
      type: AuthRefreshTokenSerialization,
    }),
  );
}
