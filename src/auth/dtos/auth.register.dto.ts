import { faker } from '@faker-js/faker';
import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import {
  IsEmail,
  IsNotEmpty,
  IsNumberString,
  IsOptional,
  IsString,
  MaxLength,
  MinLength,
} from 'class-validator';

export class RegisterDto {
  @ApiProperty({
    required: true,
    example: faker.person.firstName(),
  })
  @IsNotEmpty()
  @IsString()
  @Type(() => String)
  firstName: string;
  @ApiProperty({
    required: true,
    example: faker.person.lastName(),
  })
  @IsNotEmpty()
  @IsString()
  @Type(() => String)
  lastName: string;
  @ApiProperty({
    required: true,
    example: faker.internet.email(),
  })
  @IsNotEmpty()
  @Type(() => String)
  emailOrPhone: string;
  @ApiProperty({
    required: true,
    example: faker.internet.password(),
  })
  @IsNotEmpty()
  @Type(() => String)
  password: string;
  @ApiProperty({
    required: true,
    example: faker.internet.password(),
  })
  @IsNotEmpty()
  @Type(() => String)
  rePassword: string;
}
