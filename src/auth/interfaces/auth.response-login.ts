export interface AuthResponseLogin {
  id: string;
  accessToken: string;
  refreshToken: string;
}
