import { faker } from '@faker-js/faker';
import { ApiProperty } from '@nestjs/swagger';

export class AuthLoginSerialization {
  @ApiProperty({
    example: faker.string.uuid(),
    required: true,
    nullable: false,
  })
  readonly id: string;
  @ApiProperty({
    example: faker.string.alphanumeric(30),
    description: 'Will be valid JWT Encode string',
    required: true,
    nullable: false,
  })
  readonly accessToken: string;
  @ApiProperty({
    example: faker.string.alphanumeric(30),
    description: 'Will be valid JWT Encode string',
    required: true,
    nullable: false,
  })
  readonly refreshToken: string;
}

export class AuthRefreshTokenSerialization extends AuthLoginSerialization {}

export class AuthRegisterSerialization {
  @ApiProperty({
    example: faker.string.uuid(),
    required: true,
    nullable: false,
  })
  readonly id: string;
  @ApiProperty({
    example: faker.person.firstName(),
    required: true,
    nullable: false,
  })
  readonly firstname: string;
  @ApiProperty({
    example: faker.person.lastName(),
    required: true,
    nullable: false,
  })
  readonly lastname: string;
  @ApiProperty({
    example: faker.internet.email(),
    required: true,
    nullable: false,
  })
  readonly phoneNumberOrEmail: string;
  @ApiProperty({
    example: null,
    required: true,
    nullable: false,
  })
  readonly address: string;
  @ApiProperty({
    example: faker.internet.password(),
    required: true,
    nullable: false,
  })
  readonly password: string;
  @ApiProperty({
    example: 'user',
    required: true,
    nullable: false,
  })
  readonly role: string;
  @ApiProperty({
    example: faker.date.birthdate(),
    required: true,
    nullable: false,
  })
  readonly createdAt: Date;
  @ApiProperty({
    example: null,
    required: true,
    nullable: false,
  })
  readonly updatedAt: Date;
  @ApiProperty({
    example: null,
    required: true,
    nullable: false,
  })
  readonly deleted: number;
}
