import { Module } from '@nestjs/common';
import { AddressService } from './address.service';
import { AddressController } from './address.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from 'src/common/database/entities/User';
import { Address } from 'src/common/database/entities/Address';

@Module({
  imports: [TypeOrmModule.forFeature([User, Address])],
  controllers: [AddressController],
  providers: [AddressService],
})
export class AddressModule {}
