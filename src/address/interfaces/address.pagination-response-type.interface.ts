import { Address } from '../../common/database/entities/Address';
export interface AddressPaginationResponseType {
  data: Address[];
  total: number;
  currentPage: number;
  itemsPerPage: number;
}
