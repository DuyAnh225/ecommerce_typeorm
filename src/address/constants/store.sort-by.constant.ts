export const SORT_BY_ADDRESS: string[] = [
  'createdAt',
  'updatedAt',
  'name',
  'userId',
  'province',
  'district',
  'ward',
];
