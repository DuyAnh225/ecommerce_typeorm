import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
} from '@nestjs/common';
import { AddressService } from './address.service';
import { ApiTags } from '@nestjs/swagger';
import { Roles } from 'src/auth/decorators/role.decorator';
import { CreateAddressDto } from './dtos/address.create.dto';
import { Address } from 'src/common/database/entities/Address';
import { UpdateAddressDto } from './dtos/address.update.dto';
import { AddressFilterType } from './interfaces/address.filter-type.interface';
import { AddressPaginationResponseType } from './interfaces/address.pagination-response-type.interface';
import { DeleteResponse } from 'src/common/helper/interfaces/helper.response-delete';

@ApiTags('Address')
@Controller('address')
export class AddressController {
  constructor(private readonly addressService: AddressService) {}

  @Post()
  @Roles('admin', 'user', 'store')
  create(@Body() body: CreateAddressDto): Promise<Address> {
    return this.addressService.create(body);
  }

  @Put(':id')
  @Roles('store', 'admin')
  update(
    @Param('id') id: string,
    @Body() body: UpdateAddressDto,
  ): Promise<Address> {
    return this.addressService.update(id, body);
  }

  @Get()
  @Roles('user', 'store', 'admin')
  getAll(
    @Query() filter: AddressFilterType,
  ): Promise<AddressPaginationResponseType> {
    return this.addressService.getList(filter);
  }

  @Get(':id')
  @Roles('admin', 'user', 'store')
  getDetail(@Param('id') id: string): Promise<Address> {
    return this.addressService.getDetail(id);
  }

  @Delete(':id')
  @Roles('admin', 'store')
  delete(@Param('id') id: string): Promise<DeleteResponse<Address>> {
    return this.addressService.delete(id);
  }
}
