import { faker } from '@faker-js/faker';
import { ApiProperty } from '@nestjs/swagger';
import { IsMobilePhone, IsNotEmpty, IsString } from 'class-validator';

export class CreateAddressDto {
  @ApiProperty({
    example: faker.person.fullName(),
    required: true,
  })
  @IsNotEmpty()
  @IsString()
  name: string;

  @ApiProperty({
    example: faker.string.uuid(),
    required: true,
  })
  @IsNotEmpty()
  @IsString()
  userId: string;

  @ApiProperty({
    example: faker.phone.number(),
    required: true,
  })
  @IsNotEmpty()
  @IsMobilePhone()
  phoneNumber: string;

  @ApiProperty({
    example: faker.location.city(),
    required: true,
  })
  @IsNotEmpty()
  @IsString()
  province: string;

  @ApiProperty({
    example: 'bắc từ liêm',
    required: true,
  })
  @IsNotEmpty()
  @IsString()
  district: string;

  @ApiProperty({
    example: 'minh khai',
    required: true,
  })
  @IsNotEmpty()
  @IsString()
  ward: string;

  @ApiProperty({
    example: faker.location.streetAddress(),
    required: true,
  })
  @IsNotEmpty()
  @IsString()
  detailAddress: string;

  @ApiProperty({
    example: faker.location.state(),
    required: true,
  })
  @IsNotEmpty()
  @IsString()
  state: string;
}
