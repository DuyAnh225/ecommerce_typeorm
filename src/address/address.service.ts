import {
  BadRequestException,
  HttpStatus,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Address } from 'src/common/database/entities/Address';
import { User } from 'src/common/database/entities/User';
import { isValidPhoneNumber } from 'src/common/helper/validatePhoneNumber';
import { Brackets, Repository } from 'typeorm';
import { CreateAddressDto } from './dtos/address.create.dto';
import { UpdateAddressDto } from './dtos/address.update.dto';
import { AddressFilterType } from './interfaces/address.filter-type.interface';
import { AddressPaginationResponseType } from './interfaces/address.pagination-response-type.interface';
import { DeleteResponse } from 'src/common/helper/interfaces/helper.response-delete';

@Injectable()
export class AddressService {
  constructor(
    @InjectRepository(Address)
    private readonly addressRepository: Repository<Address>,
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
  ) {}

  async checkBodyId(userId: string, phoneNumber: string): Promise<void> {
    const user = await this.userRepository.findOne({ where: { id: userId } });
    if (!user) {
      throw new NotFoundException({
        statusCode: HttpStatus.NOT_FOUND,
        message: 'ERROR user not found',
      });
    }

    if (!(await isValidPhoneNumber(phoneNumber))) {
      throw new BadRequestException({
        statusCode: HttpStatus.BAD_REQUEST,
        message: 'ERROR: phone number is not valid',
      });
    }
  }

  async create(data: CreateAddressDto): Promise<Address> {
    await this.checkBodyId(data.userId, data.phoneNumber);
    const newAddress = await this.addressRepository.create({
      userId: data.userId,
      name: data.name,
      phoneNumber: data.phoneNumber,
      province: data.province,
      district: data.district,
      ward: data.ward,
      detailAddress: data.detailAddress,
      state: data.state,
    });

    return await this.addressRepository.save(newAddress);
  }

  async update(id: string, data: UpdateAddressDto): Promise<Address> {
    await this.checkBodyId(data.userId, data.phoneNumber);
    const address = await this.addressRepository.findOne({ where: { id: id } });
    if (!address) {
      throw new NotFoundException({
        statusCode: HttpStatus.NOT_FOUND,
        message: 'ERROR : address not found',
      });
    }

    address.userId = data.userId;
    address.name = data.name;
    address.phoneNumber = data.phoneNumber;
    address.province = data.province;
    address.district = data.district;
    address.ward = data.ward;
    address.detailAddress = data.detailAddress;
    address.state = data.state;
    address.updatedAt = new Date();

    return await this.addressRepository.save(address);
  }

  async getList(
    filter: AddressFilterType,
  ): Promise<AddressPaginationResponseType> {
    const itemsPerPage: number = Number(filter.items_per_page) || 10;
    const page: number = filter.page || 1;
    const search: string = filter.search || '';
    const skip = page > 1 ? (page - 1) * itemsPerPage : 0;
    let sort_by: string = filter.sort_by || 'createdAt';
    const order_by = filter.order_by?.toLowerCase() === 'desc' ? 'DESC' : 'ASC';

    const userId: string = filter.userId || '';

    const query = this.addressRepository.createQueryBuilder('address');

    if (userId) {
      query.andWhere(
        new Brackets((qb) => {
          if (userId) {
            qb.andWhere('address.userId = :userId', { userId });
          }
        }),
      );
    }

    if (search) {
      query.andWhere(
        new Brackets((qb) => {
          qb.where('address.name LIKE :search', { search: `%${search}%` })
            .orWhere('address.province LIKE :search', {
              search: `%${search}%`,
            })
            .orWhere('address.district LIKE :search', {
              search: `%${search}%`,
            })
            .orWhere('address.ward LIKE :search', {
              search: `%${search}%`,
            });
        }),
      );
    }

    query.orderBy(`address.${sort_by}`, order_by);
    query.skip(skip);
    query.take(itemsPerPage);

    const [data, total] = await query.getManyAndCount();

    return {
      data,
      total,
      currentPage: page,
      itemsPerPage,
    };
  }

  async getDetail(id: string): Promise<Address> {
    const address = await this.addressRepository.findOne({ where: { id: id } });
    if (!address) {
      throw new NotFoundException({
        statusCode: HttpStatus.NOT_FOUND,
        message: 'ERROR : address not found',
      });
    }
    return address;
  }

  async delete(id: string): Promise<DeleteResponse<Address>> {
    const address = await this.addressRepository.findOne({ where: { id: id } });
    if (!address) {
      throw new NotFoundException({
        statusCode: HttpStatus.NOT_FOUND,
        message: 'ERROR : address not found',
      });
    }

    await this.addressRepository.delete({ id: id });

    return {
      message: 'delete address successful',
      data: address,
    };
  }
}
