import { applyDecorators } from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiBody,
  ApiConsumes,
  ApiOperation,
  ApiParam,
  ApiQuery,
  ApiResponse,
} from '@nestjs/swagger';
import {
  UserGetDetailSerialization,
  UserGetListSerialization,
  UserUpdateSerialization,
  UserUploadAvatarSerialization,
} from '../serializations/user.serialization';
import { faker } from '@faker-js/faker';

export function UserUpdateDoc(): MethodDecorator {
  return applyDecorators(
    ApiBearerAuth(),
    ApiOperation({ summary: 'module user' }),
    ApiResponse({
      status: 200,
      description: 'Success',
      type: UserUpdateSerialization,
    }),
  );
}

export function UserGetListDoc(): MethodDecorator {
  return applyDecorators(
    ApiBearerAuth(),
    ApiOperation({ summary: 'module user' }),
    ApiQuery({
      name: 'items_per_page',
      example: faker.number.int(20),
      type: 'number',
    }),
    ApiQuery({
      name: 'page',
      example: faker.number.int(20),
      type: 'number',
    }),
    ApiQuery({
      name: 'search',
      example: 'meat',
      type: 'string',
    }),
    ApiResponse({
      status: 200,
      description: 'Success',
      type: UserGetListSerialization,
    }),
  );
}

export function UserGetDetailDoc(): MethodDecorator {
  return applyDecorators(
    ApiBearerAuth(),
    ApiOperation({ summary: 'module user' }),
    ApiParam({
      name: 'id',
      example: faker.string.uuid(),
      type: 'string',
    }),
    ApiResponse({
      status: 200,
      description: 'Success',
      type: UserGetDetailSerialization,
    }),
  );
}

export function UserUploadAvatar(): MethodDecorator {
  return applyDecorators(
    ApiBearerAuth(),
    ApiOperation({ summary: 'module user' }),
    ApiParam({
      name: 'id',
      example: faker.string.uuid(),
      type: 'string',
    }),
    ApiConsumes('multipart/form-data'),
    ApiBody({
      schema: {
        type: 'object',
        properties: {
          file: {
            type: 'string',
            format: 'binary',
          },
        },
      },
    }),
    ApiResponse({
      status: 200,
      description: 'Success',
      type: UserUploadAvatarSerialization,
    }),
  );
}
