export interface UserGetDetailResponseType {
  id: string;
  firstname: string;
  lastname: string;
  phoneNumberOrEmail: string;
  role: string;
}
