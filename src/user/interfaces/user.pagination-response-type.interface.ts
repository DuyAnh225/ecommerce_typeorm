import { UserGetDetailResponseType } from './user.get-detail-response';

export interface UserPaginationResponseType {
  data: UserGetDetailResponseType[];
  total: number;
  currentPage: number;
  itemsPerPage: number;
}
