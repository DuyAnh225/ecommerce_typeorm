import { faker } from '@faker-js/faker';
import { ApiProperty } from '@nestjs/swagger';
import { UserPaginationResponseType } from '../interfaces/user.pagination-response-type.interface';
import { UserGetDetailResponseType } from '../interfaces/user.get-detail-response';

export class UserUpdateSerialization {
  @ApiProperty({
    example: faker.string.uuid(),
    required: true,
    nullable: false,
  })
  readonly id: string;

  @ApiProperty({
    example: faker.person.firstName(),
    required: true,
    nullable: false,
  })
  readonly firstname: string;

  @ApiProperty({
    example: faker.person.lastName(),
    required: true,
    nullable: false,
  })
  readonly lastname: string;

  @ApiProperty({
    example: faker.string.uuid(),
    required: true,
    nullable: false,
  })
  readonly store_id: string;

  @ApiProperty({
    example: faker.internet.email(),
    required: true,
    nullable: false,
  })
  readonly phoneNumberOrEmail: string;

  @ApiProperty({
    example: faker.location.streetAddress(),
    required: true,
    nullable: false,
  })
  readonly address: string;

  @ApiProperty({
    example: faker.internet.password(),
    required: true,
    nullable: false,
  })
  readonly password: string;

  @ApiProperty({
    example: faker.image.url(),
    required: true,
    nullable: false,
  })
  readonly avatar: string;

  @ApiProperty({
    example: 'user',
    required: true,
    nullable: false,
  })
  readonly role: string;

  @ApiProperty({
    example: faker.date.birthdate(),
    required: true,
    nullable: false,
  })
  readonly createdAt: Date;

  @ApiProperty({
    example: faker.date.birthdate(),
    required: true,
    nullable: false,
  })
  readonly updatedAt: Date;
}

export class UserGetListSerialization implements UserPaginationResponseType {
  @ApiProperty({
    type: [UserUpdateSerialization],
    required: true,
    nullable: false,
  })
  data: UserGetDetailResponseType[];
  @ApiProperty({
    example: faker.number.int(10),
    required: true,
    nullable: false,
  })
  total: number;
  @ApiProperty({
    example: faker.number.int(10),
    required: true,
    nullable: false,
  })
  currentPage: number;
  @ApiProperty({
    example: faker.number.int(10),
    required: true,
    nullable: false,
  })
  itemsPerPage: number;
}

export class UserGetDetailSerialization extends UserUpdateSerialization {}

export class UserUploadAvatarSerialization extends UserUpdateSerialization {}
