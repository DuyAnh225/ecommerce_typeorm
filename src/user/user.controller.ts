import {
  Body,
  Controller,
  Param,
  Put,
  Query,
  Get,
  UploadedFile,
  Post,
} from '@nestjs/common';
import { UserService } from './user.service';
import { ApiConsumes, ApiTags, ApiBody } from '@nestjs/swagger';
import { Roles } from 'src/auth/decorators/role.decorator';
import { UpdateUserDto } from './dtos/user.update.dto';
import { User } from 'src/common/database/entities/User';
import { UserFilterType } from './interfaces/user.filter-type.interface';
import { UserPaginationResponseType } from './interfaces/user.pagination-response-type.interface';
import { UserGetDetailResponseType } from './interfaces/user.get-detail-response';
import { UseInterceptors } from '@nestjs/common';
import {
  UserGetDetailDoc,
  UserGetListDoc,
  UserUpdateDoc,
} from './docs/user.doc';
import { FileInterceptor } from '@nestjs/platform-express';
import { UserUploadAvatar } from './docs/user.doc';

@ApiTags('Users')
@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @UserUpdateDoc()
  @Put(':id')
  @Roles('user', 'store', 'admin')
  update(@Param('id') id: string, @Body() body: UpdateUserDto): Promise<User> {
    return this.userService.update(id, body);
  }

  @UserGetListDoc()
  @Get()
  @Roles('admin')
  getAll(@Query() filter: UserFilterType): Promise<UserPaginationResponseType> {
    return this.userService.getList(filter);
  }

  @UserGetDetailDoc()
  @Get(':id')
  @Roles('user', 'store', 'admin')
  getDetail(@Param('id') id: string): Promise<UserGetDetailResponseType> {
    return this.userService.getDetail(id);
  }

  @UserUploadAvatar()
  @Post('upload/:id')
  @Roles('user', 'store', 'admin')
  
  @UseInterceptors(FileInterceptor('file'))
  upload(
    @Param('id') id: string,
    @UploadedFile() file: Express.Multer.File,
  ): Promise<User> {
    return this.userService.uploadAvatar(id, file);
  }
}
