import {
  BadRequestException,
  HttpStatus,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/common/database/entities/User';
import { Brackets, Like, Repository } from 'typeorm';
import { UpdateUserDto } from './dtos/user.update.dto';
import { UserFilterType } from './interfaces/user.filter-type.interface';
import { UserPaginationResponseType } from './interfaces/user.pagination-response-type.interface';
import { SORT_BY_USER } from './constants/user.sort-by.constant';
import { UserGetDetailResponseType } from './interfaces/user.get-detail-response';
import { MediaService } from 'src/media/media.service';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User) private readonly userRepository: Repository<User>,

    private readonly mediaService: MediaService,
  ) {}

  async update(id: string, userData: UpdateUserDto): Promise<User> {
    const user = await this.userRepository.findOne({
      where: { id: id },
    });
    if (!user) {
      throw new NotFoundException({
        statusCode: HttpStatus.NOT_FOUND,
        message: 'product does not exist',
      });
    }

    id;
    user.firstname = userData.firstname;
    user.lastname = userData.lastname;
    user.updatedAt = new Date();
    return await this.userRepository.save(user);
  }

  async getList(filter: UserFilterType): Promise<UserPaginationResponseType> {
    const itemsPerPage: number = Number(filter.items_per_page) || 10;
    const page: number = filter.page || 1;
    const search: string = filter.search || '';
    const skip = page - 1 ? (page - 1) * itemsPerPage : 0;
    let sort_by: string = filter.sort_by || 'created_at';
    const order_by = filter.order_by?.toLowerCase() === 'desc' ? 'DESC' : 'ASC';

    if (!SORT_BY_USER.includes(sort_by)) {
      sort_by = 'createdAt';
    }

    const query = this.userRepository.createQueryBuilder('user');

    if (search) {
      query.andWhere(
        new Brackets((qb) => {
          qb.where('user.firstname LIKE :search', { search: `%${search}%` })
            .orWhere('user.lastname LIKE :search', {
              search: `%${search}%`,
            })
            .orWhere('user.phoneNumberOrEmail LIKE :search', {
              search: `%${search}%`,
            })
            .orWhere('user.address LIKE :search', {
              search: `%${search}%`,
            });
        }),
      );
    }

    query.orderBy(`user.${sort_by}`, order_by);
    query.skip(skip);
    query.take(itemsPerPage);

    const [data, total] = await query.getManyAndCount();

    return {
      data,
      total,
      currentPage: page,
      itemsPerPage,
    };
  }

  async getDetail(id: string): Promise<UserGetDetailResponseType> {
    const user = await this.userRepository.findOne({
      where: { id: id },
    });
    if (!user) {
      throw new NotFoundException({
        statusCode: HttpStatus.NOT_FOUND,
        message: 'user not found',
      });
    }
    return {
      id: user.id,
      firstname: user.firstname,
      lastname: user.lastname,
      phoneNumberOrEmail: user.phoneNumberOrEmail,
      role: user.role,
    };
  }

  async uploadAvatar(id: string, file: Express.Multer.File): Promise<User> {
    const user = await this.userRepository.findOne({
      where: { id: id },
    });
    if (!user) {
      throw new NotFoundException({
        statusCode: HttpStatus.NOT_FOUND,
        message: 'user not found',
      });
    }

    const location = await this.mediaService.upload(file);
    user.avatar = location;

    return await this.userRepository.save(user);
  }
}
