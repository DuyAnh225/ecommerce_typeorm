export const SORT_BY_USER: string[] = [
  'createdAt',
  'updatedAt',
  'firstname',
  'lastname',
  'phoneNumberOrEmail',
  'address',
];
