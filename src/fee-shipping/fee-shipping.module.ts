import { Module } from '@nestjs/common';
import { FeeShippingService } from './fee-shipping.service';
import { FeeShippingController } from './fee-shipping.controller';
import { ConfigService } from '@nestjs/config';

@Module({
  controllers: [FeeShippingController],
  providers: [FeeShippingService, ConfigService],
})
export class FeeShippingModule {}
