import {
  BadRequestException,
  HttpStatus,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import axios, { AxiosRequestConfig } from 'axios';
import { CalculateFeeShippingDto } from './dtos/fee-shipping.calculate.dto';

import { removeDiacriticsAndSpaces } from 'src/common/helper/removeDiacriticsAndSpaces';

@Injectable()
export class FeeShippingService {
  private readonly api_key;
  private readonly url_api_province;
  private readonly url_api_district;
  private readonly url_api_ward;
  private readonly url_api_fee_shipping;
  private readonly shop_id;
  private readonly config: AxiosRequestConfig;
  private readonly service_type_id;

  constructor(private readonly configService: ConfigService) {
    this.api_key = this.configService.get('API_KEY');
    this.url_api_province = this.configService.get('URL_API_PROVINCE');
    this.url_api_district = this.configService.get('URL_API_DISTRICT');
    this.url_api_ward = this.configService.get('URL_API_WARD');
    this.url_api_fee_shipping = this.configService.get('URL_API_FEE_SHIPPING');
    this.shop_id = this.configService.get('SHOP_ID');
    this.service_type_id = 2;

    this.config = {
      headers: {
        'Content-Type': 'application/json',
        token: this.api_key,
        ShopId: this.shop_id,
      },
    };
  }

  async getProvinceId(provinceName: string): Promise<number> {
    try {
      const response = await axios.get(this.url_api_province, this.config);
      const data = response.data.data;
      const foundProvince = data.find((province) =>
        province.NameExtension.some(async (name) =>
          name
            .toLowerCase()
            .includes(await removeDiacriticsAndSpaces(provinceName)),
        ),
      );

      if (foundProvince) {
        return foundProvince.ProvinceID;
      } else {
        throw new NotFoundException({
          statusCode: HttpStatus.NOT_FOUND,
          message: `Province/City "${provinceName}" not found.`,
        });
      }
    } catch (error) {
      throw new BadRequestException({
        statusCode: HttpStatus.BAD_REQUEST,
        message: `Error getting province/city data: ${error.message}`,
      });
    }
  }

  async getDistrictId(
    provinceName: string,
    districtName: string,
  ): Promise<number> {
    const provinceId = await this.getProvinceId(provinceName);

    const data = {
      province_id: provinceId,
    };
    try {
      const response = await axios.post(
        this.url_api_district,
        data,
        this.config,
      );
      const districts = response.data.data;
      const foundDistrict = districts.find((district) =>
        district.NameExtension.some(async (name) =>
          name
            .toLowerCase()
            .includes(await removeDiacriticsAndSpaces(districtName)),
        ),
      );
      if (foundDistrict) {
        return foundDistrict.DistrictID;
      } else {
        throw new NotFoundException({
          statusCode: HttpStatus.NOT_FOUND,
          message: `District "${districtName}" not found.`,
        });
      }
    } catch (error) {
      throw new BadRequestException({
        statusCode: HttpStatus.BAD_REQUEST,
        message: `Error getting district data: ${error.message}`,
      });
    }
  }

  async getWardCode(
    provinceName: string,
    districtName: string,
    wardName: string,
  ): Promise<string> {
    const districtId = await this.getDistrictId(provinceName, districtName);
    const data = {
      district_id: districtId,
    };

    try {
      const response = await axios.post(this.url_api_ward, data, this.config);
      const wards = response.data.data;
      const foundWards = wards.find((district) =>
        district.NameExtension.some(async (name) =>
          name
            .toLowerCase()
            .includes(await removeDiacriticsAndSpaces(wardName)),
        ),
      );
      if (foundWards) {
        return foundWards.WardCode;
      } else {
        throw new NotFoundException({
          statusCode: HttpStatus.NOT_FOUND,
          message: `Ward "${wardName}" not found.`,
        });
      }
    } catch (error) {
      throw new BadRequestException({
        statusCode: HttpStatus.BAD_REQUEST,
        message: `Error getting ward data: ${error.message}`,
      });
    }
  }

  async calculateFeeShipping(data: CalculateFeeShippingDto): Promise<number> {
    const from_district_id = await this.getDistrictId(
      data.from_province_name,
      data.from_district_name,
    );
    const from_ward_code = await this.getWardCode(
      data.from_province_name,
      data.from_district_name,
      data.from_ward_name,
    );

    const to_district_id = await this.getDistrictId(
      data.to_province_name,
      data.to_district_name,
    );
    const to_ward_code = await this.getWardCode(
      data.to_province_name,
      data.to_district_name,
      data.to_ward_name,
    );

    const dataCalculateFee = {
      service_type_id: this.service_type_id,
      from_district_id: from_district_id,
      from_ward_code: from_ward_code,
      to_district_id: to_district_id,
      to_ward_code: to_ward_code,
      weight: data.weight,
      items: data.items,
    };

    try {
      const response = await axios.post(
        this.url_api_fee_shipping,
        dataCalculateFee,
        this.config,
      );
      return response.data.data.total;
    } catch (error) {
      throw new BadRequestException({
        statusCode: HttpStatus.BAD_REQUEST,
        message: `Error calculate shipping fees: ${error.message}`,
      });
    }
  }
}
