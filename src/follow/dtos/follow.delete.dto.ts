import { CreateFollowDto } from './follow.create.dto';

export class DeleteFollowDto extends CreateFollowDto {}
