import { Body, Controller, Post, Get, Query } from '@nestjs/common';
import { FollowService } from './follow.service';
import { ApiTags } from '@nestjs/swagger';
import { Roles } from 'src/auth/decorators/role.decorator';
import { CreateFollowDto } from './dtos/follow.create.dto';
import { FollowFilterType } from './interfaces/follow.filter-type.interface';
import { FollowPaginationResponseType } from './interfaces/follow.pagination-response-type.interface';
import { FollowDoc, FollowGetListDoc } from './docs/follow.doc';

@ApiTags('Follow')
@Controller('follow')
export class FollowController {
  constructor(private readonly followService: FollowService) {}

  @FollowDoc()
  @Post()
  @Roles('user', 'admin', 'store')
  follow(@Body() body: CreateFollowDto): Promise<string> {
    return this.followService.follow(body);
  }

  @FollowGetListDoc()
  @Get()
  @Roles('user', 'store', 'admin')
  getAll(
    @Query() filter: FollowFilterType,
  ): Promise<FollowPaginationResponseType> {
    return this.followService.getAll(filter);
  }
}
