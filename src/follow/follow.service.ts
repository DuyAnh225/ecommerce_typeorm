import { HttpStatus, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Follow } from 'src/common/database/entities/Follow';
import { Brackets, Repository } from 'typeorm';
import { User } from 'src/common/database/entities/User';
import { Store } from 'src/common/database/entities/Store';
import { CreateFollowDto } from './dtos/follow.create.dto';
import { DeleteFollowDto } from './dtos/follow.delete.dto';
import { FollowFilterType } from './interfaces/follow.filter-type.interface';
import { FollowPaginationResponseType } from './interfaces/follow.pagination-response-type.interface';
import { SORT_BY_FOLLOW } from './constants/follow.sort-by.constant';

@Injectable()
export class FollowService {
  constructor(
    @InjectRepository(Follow)
    private readonly followRepository: Repository<Follow>,
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
    @InjectRepository(Store)
    private readonly storeRepository: Repository<Store>,
  ) {}

  private async checkBodyId(data: CreateFollowDto): Promise<void> {
    const user = await this.userRepository.findOne({
      where: { id: data.userId },
    });

    if (!user) {
      throw new NotFoundException({
        statusCode: HttpStatus.NOT_FOUND,
        message: 'ERROR : user not found',
      });
    }

    const store = await this.storeRepository.findOne({
      where: { id: data.storeId },
    });

    if (!store) {
      throw new NotFoundException({
        statusCode: HttpStatus.NOT_FOUND,
        message: 'ERROR : store not found',
      });
    }
  }

  async create(followData: CreateFollowDto): Promise<Follow> {
    const newFollow = await this.followRepository.create({
      userId: followData.userId,
      storeId: followData.storeId,
    });

    return await this.followRepository.save(newFollow);
  }

  async delete(followData: DeleteFollowDto): Promise<void> {
    const abc = await this.followRepository.delete({
      userId: followData.userId,
      storeId: followData.storeId,
    });
  }

  async follow(followData: CreateFollowDto): Promise<any> {
    await this.checkBodyId(followData);
    const follow = await this.followRepository.findOne({
      where: {
        userId: followData.userId,
        storeId: followData.storeId,
      },
    });
    if (!follow) {
      return await this.create(followData);
    } else {
      await this.delete(followData);
      return {
        message: 'delete follow successful',
        data: follow,
      };
    }
  }

  async getAll(
    filter: FollowFilterType,
  ): Promise<FollowPaginationResponseType> {
    const itemsPerPage: number = Number(filter.items_per_page) || 10;
    const page: number = filter.page || 1;
    const search: string = filter.search || '';
    const skip = page - 1 ? (page - 1) * itemsPerPage : 0;
    let sort_by: string = filter.sort_by || 'created_at';
    const order_by = filter.order_by?.toLowerCase() === 'desc' ? 'DESC' : 'ASC';

    if (!SORT_BY_FOLLOW.includes(sort_by)) {
      sort_by = 'createdAt';
    }

    const userId: string = filter.userId || '';
    const storeId: string = filter.storeId || '';

    const query = this.followRepository.createQueryBuilder('follow');

    if (userId || storeId) {
      query.andWhere(
        new Brackets((qb) => {
          if (userId) {
            qb.andWhere('follow.userId = :userId', { userId });
          }
          if (storeId) {
            qb.andWhere('follow.storeId = :storeId', { storeId });
          }
        }),
      );
    }
    query.orderBy(`follow.${sort_by}`, order_by);
    query.skip(skip);
    query.take(itemsPerPage);

    const [data, total] = await query.getManyAndCount();

    return {
      data,
      total,
      currentPage: page,
      itemsPerPage,
    };
  }
}
