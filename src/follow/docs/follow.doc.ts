import { applyDecorators } from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiQuery,
  ApiResponse,
} from '@nestjs/swagger';
import {
  FollowCreateSerialization,
  FollowDeleteSerialization,
  FollowGetListSerialization,
} from '../serializations/follow.serialization';
import { faker } from '@faker-js/faker';

export function FollowDoc(): MethodDecorator {
  return applyDecorators(
    ApiBearerAuth(),
    ApiOperation({ summary: 'module follow' }),
    ApiResponse({
      status: 201,
      description: 'Success',
      type: FollowCreateSerialization,
    }),
    ApiResponse({
      status: 200,
      description: 'Delete Success',
      type: FollowDeleteSerialization,
    }),
  );
}

export function FollowGetListDoc(): MethodDecorator {
  return applyDecorators(
    ApiBearerAuth(),
    ApiOperation({ summary: 'module store' }),
    ApiQuery({
      name: 'items_per_page',
      example: faker.number.int(20),
      type: 'number',
      required: false,
    }),
    ApiQuery({
      name: 'page',
      example: faker.number.int(20),
      type: 'number',
      required: false,
    }),
    ApiQuery({
      name: 'userId',
      example: faker.string.uuid(),
      type: 'string',
      required: false,
    }),
    ApiQuery({
      name: 'storeId',
      example: faker.string.uuid(),
      type: 'string',
      required: false,
    }),
    ApiResponse({
      status: 200,
      description: 'Success',
      type: FollowGetListSerialization,
    }),
  );
}
