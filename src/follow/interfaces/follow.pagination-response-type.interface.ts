import { Follow } from 'src/common/database/entities/Follow';

export interface FollowPaginationResponseType {
  data: Follow[];
  total: number;
  currentPage: number;
  itemsPerPage: number;
}
