export interface FollowFilterType {
  items_per_page?: number;
  page?: number;
  search?: string;
  sort_by?: string;
  order_by?: string;
  userId?: string;
  storeId?: string;
}
