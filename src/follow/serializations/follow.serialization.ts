import { ApiProperty } from '@nestjs/swagger';
import { faker } from '@faker-js/faker';
import { DeleteResponse } from 'src/common/helper/interfaces/helper.response-delete';
import { Follow } from 'src/common/database/entities/Follow';
import { FollowPaginationResponseType } from '../interfaces/follow.pagination-response-type.interface';

export class FollowCreateSerialization {
  @ApiProperty({
    example: faker.string.uuid(),
    required: true,
    nullable: false,
  })
  readonly userId: string;

  @ApiProperty({
    example: faker.string.uuid(),
    required: true,
    nullable: false,
  })
  readonly storeId: string;

  @ApiProperty({
    example: faker.date.birthdate(),
    required: true,
    nullable: false,
  })
  readonly createdAt: Date;

  @ApiProperty({
    example: null,
    required: true,
    nullable: false,
  })
  readonly updatedAt: Date;
}

export class FollowDeleteSerialization implements DeleteResponse<Follow> {
  @ApiProperty({
    example: 'delete follow successful',
  })
  message: string;
  @ApiProperty({
    type: [FollowCreateSerialization],
    required: true,
    nullable: false,
  })
  data: Follow;
}

export class FollowGetListSerialization
  implements FollowPaginationResponseType
{
  @ApiProperty({
    type: [FollowCreateSerialization],
    required: true,
    nullable: false,
  })
  data: Follow[];

  @ApiProperty({
    example: faker.number.int(10),
    required: true,
    nullable: false,
  })
  total: number;

  @ApiProperty({
    example: faker.number.int(10),
    required: true,
    nullable: false,
  })
  currentPage: number;

  @ApiProperty({
    example: faker.number.int(10),
    required: true,
    nullable: false,
  })
  itemsPerPage: number;
}
