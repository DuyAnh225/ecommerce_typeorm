import { Module } from '@nestjs/common';
import { FollowService } from './follow.service';
import { FollowController } from './follow.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Follow } from 'src/common/database/entities/Follow';
import { User } from 'src/common/database/entities/User';
import { Store } from 'src/common/database/entities/Store';

@Module({
  imports: [TypeOrmModule.forFeature([Follow, User, Store])],
  controllers: [FollowController],
  providers: [FollowService],
})
export class FollowModule {}
