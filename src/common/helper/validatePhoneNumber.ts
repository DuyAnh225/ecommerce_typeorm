export async function isValidPhoneNumber(
  phoneNumber: string,
): Promise<boolean> {
  const phoneRegex: RegExp = /^(0[1-9])+([0-9]{8,9})\b/;

  return phoneRegex.test(phoneNumber);
}
