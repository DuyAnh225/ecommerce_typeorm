export interface DeleteResponse<T> {
  message: string;
  data: T;
}
