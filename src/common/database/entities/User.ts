import {
  Column,
  Entity,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Cart } from './Cart';
import { Feedback } from './Feedback';
import { Order } from './Order';
import { Payment } from './Payment';
import { Store } from './Store';
import { Token } from './Token';
import { Wishlist } from './WishList';
import { Address } from './Address';
import { Follow } from './Follow';

@Entity('user')
export class User {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('varchar', { name: 'firstname', length: 50 })
  firstname: string;

  @Column('varchar', { name: 'lastname', length: 50 })
  lastname: string;

  @Column('varchar', {
    name: 'phone_number_or_email',
    unique: true,
    length: 20,
  })
  phoneNumberOrEmail: string;

  @Column('varchar', { name: 'password', length: 200 })
  password: string;

  @Column('varchar', { name: 'avatar', length: 200, nullable: true })
  avatar: string;

  @Column('varchar', { name: 'role', length: 10 })
  role: string;

  @Column('datetime', {
    name: 'created_at',
    default: () => 'CURRENT_TIMESTAMP',
  })
  createdAt: Date;

  @Column('datetime', { name: 'updated_at', nullable: true })
  updatedAt: Date;

  @OneToMany(() => Cart, (cart) => cart.user)
  cart: Cart;

  @OneToMany(() => Address, (address) => address.user)
  address: Address;

  @OneToMany(() => Feedback, (feedback) => feedback.user)
  feedbacks: Feedback[];

  @OneToMany(() => Order, (order) => order.user)
  orders: Order[];

  @OneToMany(() => Payment, (payment) => payment.user)
  payments: Payment[];

  @OneToMany(() => Store, (store) => store.user)
  stores: Store[];

  @OneToMany(() => Token, (token) => token.user)
  tokens: Token[];

  @OneToMany(() => Wishlist, (wishlist) => wishlist.user)
  wishlists: Wishlist[];

  @OneToMany(() => Follow, (follow) => follow.user)
  follows: Follow[];
}
