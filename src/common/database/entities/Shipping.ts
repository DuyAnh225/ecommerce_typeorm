import { Column, Entity, OneToMany, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import { Order } from "./Order";

@Entity("shipping", { schema: "ecommerceda" })
export class Shipping {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column("varchar", { name: "name", nullable: true, length: 50 })
  name: string ;

  @Column("int", { name: "price" })
  price: number ;

  @Column("datetime", {
    name: "created_at",
    nullable: true,
    default: () => "CURRENT_TIMESTAMP",
  })
  createdAt: Date ;

  @Column("datetime", { name: "updated_at", nullable: true })
  updatedAt: Date ;

  @OneToOne(() => Order, (order) => order.shipping)
  order: Order;
}
