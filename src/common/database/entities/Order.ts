import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Cart } from './Cart';
import { User } from './User';
import { Payment } from './Payment';
import { Shipping } from './Shipping';
import { Address } from './Address';
import { OrderStatus } from '../../../order/constants/order.status.constant';

@Index('payment_id', ['paymentId'], {})
@Index('shipping_id', ['shippingId'], {})
@Index('user_id', ['userId'], {})
@Entity('order', { schema: 'ecommerceda' })
export class Order {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('uuid', { name: 'user_id' })
  userId: string;

  @Column('uuid', { name: 'address_id', nullable: true })
  addressId: string;

  @Column('varchar', { name: 'note', nullable: true, length: 1000 })
  note: string;

  @Column('varchar', { name: 'order_code', nullable: true, length: 200 })
  orderCode: string;

  @Column('enum', {
    name: 'status',
    enum: OrderStatus,
    nullable: true,
  })
  status: OrderStatus = OrderStatus.ReadyToPick;

  @Column('datetime', { name: 'order_date', nullable: true })
  orderDate: Date;

  @Column('enum', {
    name: 'payment_method',
    enum: ['COD', 'ONLINE'],
    default: 'COD',
    nullable: true,
  })
  paymentMethod: 'COD' | 'ONLINE';

  @Column('uuid', { name: 'payment_id', nullable: true })
  paymentId: string;

  @Column('int', { name: 'total_money', nullable: true })
  totalMoney: number;

  @Column('uuid', { name: 'shipping_id', nullable: true })
  shippingId: string;

  @Column('datetime', { name: 'expected_delivery_time', nullable: true })
  expectedDeliveryTime: Date;

  @Column('datetime', {
    name: 'created_at',
    nullable: true,
    default: () => 'CURRENT_TIMESTAMP',
  })
  createdAt: Date;

  @Column('datetime', { name: 'updated_at', nullable: true })
  updatedAt: Date;

  @OneToMany(() => Cart, (cart) => cart.order)
  carts: Cart[];

  @ManyToOne(() => User, (user) => user.orders, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION',
  })
  @JoinColumn([{ name: 'user_id', referencedColumnName: 'id' }])
  user: User;

  @ManyToOne(() => Payment, (payment) => payment.orders, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION',
  })
  @JoinColumn([{ name: 'payment_id', referencedColumnName: 'id' }])
  payment: Payment;

  @OneToOne(() => Shipping, (shipping) => shipping.order)
  @JoinColumn([{ name: 'shipping_id', referencedColumnName: 'id' }])
  shipping: Shipping;

  @ManyToOne(() => Address, (address) => address.order, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION',
  })
  @JoinColumn([{ name: 'address_id', referencedColumnName: 'id' }])
  address: Address;
}
