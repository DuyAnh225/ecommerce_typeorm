import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { User } from './User';
import { Order } from './Order';

@Entity('address')
export class Address {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('varchar', { name: 'name', nullable: true, length: 100 })
  name: string;

  @Column('uuid', { name: 'user_id', nullable: true })
  userId: string;

  @Column('varchar', {
    name: 'phone_number',
    length: 20,
  })
  phoneNumber: string;

  @Column('varchar', { name: 'province', nullable: true, length: 100 })
  province: string;

  @Column('varchar', { name: 'district', nullable: true, length: 100 })
  district: string;

  @Column('varchar', { name: 'ward', nullable: true, length: 100 })
  ward: string;

  @Column('varchar', { name: 'detail_address', nullable: true, length: 500 })
  detailAddress: string;

  @Column('varchar', { name: 'state', nullable: true, length: 100 })
  state: string;

  @Column('datetime', {
    name: 'created_at',
    default: () => 'CURRENT_TIMESTAMP',
  })
  createdAt: Date;

  @Column('datetime', { name: 'updated_at', nullable: true })
  updatedAt: Date;

  @ManyToOne(() => User, (user) => user.address, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION',
  })
  @JoinColumn([{ name: 'user_id', referencedColumnName: 'id' }])
  user: User;

  @OneToMany(() => Order, (order) => order.address)
  order: Order;
}
