import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from "typeorm";
import { User } from "./User";
import { Product } from "./Product";

@Index("product_id", ["productId"], {})
@Index("user_id", ["userId"], {})
@Entity("feedback", { schema: "ecommerceda" })
export class Feedback {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column("uuid", { name: "user_id", nullable: true })
  userId: string ;

  @Column("uuid", { name: "product_id", nullable: true })
  productId: string ;

  @Column("int", { name: "rating", nullable: true })
  rating: number ;

  @Column("varchar", { name: "comment", nullable: true, length: 1000 })
  comment: string ;

  @Column("int", { name: "status", nullable: true, default: () => "'0'" })
  status: number ;

  @Column("datetime", {
    name: "created_at",
    nullable: true,
    default: () => "CURRENT_TIMESTAMP",
  })
  createdAt: Date ;

  @Column("datetime", { name: "updated_at", nullable: true })
  updatedAt: Date ;

  @ManyToOne(() => User, (user) => user.feedbacks, {
    onDelete: "NO ACTION",
    onUpdate: "NO ACTION",
  })
  @JoinColumn([{ name: "user_id", referencedColumnName: "id" }])
  user: User;

  @ManyToOne(() => Product, (product) => product.feedbacks, {
    onDelete: "NO ACTION",
    onUpdate: "NO ACTION",
  })
  @JoinColumn([{ name: "product_id", referencedColumnName: "id" }])
  product: Product;
}
