import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Order } from './Order';
import { User } from './User';

@Index('user_id', ['userId'], {})
@Entity('payment', { schema: 'ecommerceda' })
export class Payment {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('uuid', { name: 'user_id', nullable: true })
  userId: string;

  @Column('datetime', { name: 'date_payment', nullable: true })
  datePayment: Date;

  @Column('enum', {
    name: 'payment_method',
    enum: ['COD', 'ONLINE'],
    default: 'COD',
  })
  paymentMethod: 'COD' | 'ONLINE';

  @Column('varchar', { name: 'card_id', nullable: true, length: 50 })
  cardId: string;

  @Column('varchar', { name: 'last4', nullable: true, length: 50 })
  last4: string;

  @Column('varchar', { name: 'brand', nullable: true, length: 50 })
  brand: string;

  @Column('datetime', {
    name: 'created_at',
    nullable: true,
    default: () => 'CURRENT_TIMESTAMP',
  })
  createdAt: Date;

  @Column('datetime', { name: 'updated_at', nullable: true })
  updatedAt: Date;

  @OneToMany(() => Order, (order) => order.payment)
  orders: Order[];

  @ManyToOne(() => User, (user) => user.payments, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION',
  })
  @JoinColumn([{ name: 'user_id', referencedColumnName: 'id' }])
  user: User;
}
