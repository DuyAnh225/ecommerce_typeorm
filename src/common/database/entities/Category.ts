import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from "typeorm";
import { Product } from "./Product";

@Entity("category", { schema: "ecommerceda" })
export class Category {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column("varchar", { name: "name", nullable: true, length: 100 })
  name: string ;

  @Column("longtext", { name: "description", nullable: true })
  description: string ;

  @Column("datetime", {
    name: "created_at",
    nullable: true,
    default: () => "CURRENT_TIMESTAMP",
  })
  createdAt: Date ;

  @Column("datetime", { name: "updated_at", nullable: true })
  updatedAt: Date ;

  @OneToMany(() => Product, (product) => product.category)
  products: Product[];
}
