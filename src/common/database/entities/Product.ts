import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { CartItem } from './CartItem';
import { Feedback } from './Feedback';
import { Galery } from './Galery';
import { Category } from './Category';
import { Store } from './Store';
import { Wishlist } from './WishList';
import { Media } from './Media';

@Index('category_id', ['categoryId'], {})
@Index('store_id', ['storeId'], {})
@Entity('product', { schema: 'ecommerceda' })
export class Product {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('uuid', { name: 'category_id', nullable: true })
  categoryId: string;

  @Column('uuid', { name: 'store_id', nullable: true })
  storeId: string;

  @Column('varchar', { name: 'title', nullable: true, length: 250 })
  title: string;
  @Column('int', { name: 'inventory', nullable: true })
  inventory: number;
  @Column('int', { name: 'price', nullable: true })
  price: number;

  @Column('int', { name: 'weight', nullable: true })
  weight: number;

  @Column('int', { name: 'offerPrice', nullable: true })
  offerPrice: number;

  @Column('varchar', { name: 'thumbnail', nullable: true, length: 500 })
  thumbnail: string;

  @Column('longtext', { name: 'description', nullable: true })
  description: string;

  @Column('longtext', { name: 'location_detail', nullable: true })
  locationDetail: string;

  @Column('varchar', { name: 'price_type', nullable: true, length: 500 })
  priceType: string;

  @Column('longtext', { name: 'additional_detail', nullable: true })
  additionalDetail: string;

  @Column('datetime', {
    name: 'created_at',
    nullable: true,
    default: () => 'CURRENT_TIMESTAMP',
  })
  createdAt: Date;

  @Column('datetime', { name: 'updated_at', nullable: true })
  updatedAt: Date;

  @OneToMany(() => CartItem, (cartItem) => cartItem.product)
  cartItems: CartItem[];

  @OneToMany(() => Feedback, (feedback) => feedback.product)
  feedbacks: Feedback[];

  @OneToMany(() => Galery, (galery) => galery.product)
  galeries: Galery[];

  @OneToMany(() => Wishlist, (wishlist) => wishlist.product)
  wishlists: Wishlist[];

  @OneToMany(() => Media, (media) => media.product)
  medias: Media[];

  @ManyToOne(() => Category, (category) => category.products, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION',
  })
  @JoinColumn([{ name: 'category_id', referencedColumnName: 'id' }])
  category: Category;

  @ManyToOne(() => Store, (store) => store.products, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION',
  })
  @JoinColumn([{ name: 'store_id', referencedColumnName: 'id' }])
  store: Store;
}
