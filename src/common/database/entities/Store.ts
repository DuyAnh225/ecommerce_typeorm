import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Product } from './Product';
import { User } from './User';
import { Follow } from './Follow';

@Index('user_id', ['userId'], {})
@Entity('store', { schema: 'ecommerceda' })
export class Store {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('uuid', { name: 'user_id' })
  userId: string;

  @Column('varchar', { name: 'name', length: 50 })
  name: string;

  @Column('varchar', {
    name: 'phone_number',
    length: 12,
  })
  phoneNumber: string;

  @Column('varchar', { name: 'web_address', length: 100 })
  webAddress: string;

  @Column('longtext', { name: 'description', nullable: true })
  description: string;

  @Column('varchar', { name: 'type', nullable: true, length: 50 })
  type: string;

  @Column('varchar', { name: 'address_detail', nullable: true, length: 500 })
  addressDetail: string;

  @Column('varchar', { name: 'avatar', length: 200, nullable: true })
  avatar: string;

  @Column('varchar', { name: 'province', nullable: true, length: 50 })
  province: string;
  @Column('varchar', { name: 'district', nullable: true, length: 50 })
  district: string;
  @Column('varchar', { name: 'ward', nullable: true, length: 50 })
  ward: string;

  @Column('varchar', { name: 'country', nullable: true, length: 30 })
  country: string;

  @Column('varchar', { name: 'courier_name', nullable: true, length: 50 })
  courierName: string;

  @Column('longtext', { name: 'tag_line', nullable: true })
  tagLine: string;

  @Column('datetime', {
    name: 'created_at',
    default: () => 'CURRENT_TIMESTAMP',
  })
  createdAt: Date;

  @Column('datetime', { name: 'updated_at', nullable: true })
  updatedAt: Date;

  @OneToMany(() => Product, (product) => product.store)
  products: Product[];

  @ManyToOne(() => User, (user) => user.stores, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION',
  })
  @JoinColumn([{ name: 'user_id', referencedColumnName: 'id' }])
  user: User;

  @OneToMany(() => Follow, (follow) => follow.store)
  follows: Follow[];
}
