import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from "typeorm";
import { Product } from "./Product";

@Index("product_id", ["productId"], {})
@Entity("galery", { schema: "ecommerceda" })
export class Galery {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column("uuid", { name: "product_id", nullable: true })
  productId: string ;

  @Column("varchar", { name: "thumbnail", nullable: true, length: 500 })
  thumbnail: string ;

  @Column("datetime", {
    name: "created_at",
    nullable: true,
    default: () => "CURRENT_TIMESTAMP",
  })
  createdAt: Date ;

  @Column("datetime", { name: "updated_at", nullable: true })
  updatedAt: Date ;

  @ManyToOne(() => Product, (product) => product.galeries, {
    onDelete: "NO ACTION",
    onUpdate: "NO ACTION",
  })
  @JoinColumn([{ name: "product_id", referencedColumnName: "id" }])
  product: Product;
}
